#!/usr/bin/env python3

# data source = https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.json

# import packages dummy
import json
import argparse
    # this is for checking and clearing old json
import os

###---/ set the globals /---###
#   import the data
parser = argparse.ArgumentParser()

#   arguments & parsing
parser.add_argument('-d', '--data', nargs='*')
args = parser.parse_args()

def pull_data_source():
    try:
        args.data[0]
    except:
        data_source = 'data/index.json'
    else:
        data_source = args.data[0]
    print(data_source)
    return(data_source)

def set_json(data_source):
    try:
        with open(data_source) as json_file:
            data = json.load(json_file)
            return(data)
    except:
        print('An error occured loading json. Check for: {}'.format(data_source))
        quit()


def print_output(data):
    print(data['formatVersion'])
    print('\n=====   ///   =====\n')

    skus = list(data['products'].keys())

    print(len(skus))

    #print(data['products'][skus[0]])

    return(skus)
def get_keys(sku_object, pulledKeys = None ):
    if pulledKeys is None:
        keys = list(sku_object.keys())
        return(keys)
    else:
        obj_keys = list(sku_object.keys())
        for key in obj_keys:
            if key in pulledKeys:
                pass
            else:
                print(key)
                pulledKeys.append(key)
        return(pulledKeys)

def match_key_sku(keys, sku):
    print('=+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++==+++++=')
    for key in keys:
        try:
            print('{}: {}'.format(key, sku[key]))
        except:
            print('{}: NO MATCH'.format(key))

def create_json(sku):
    if(sku['servicecode'] == "AmazonEC2"):
        print(sku)
    else:
        pass

def delete_file_if_exists(filename):
    if(os.path.isfile(filename)):
        os.remove(filename)

def output_json(skus, json_data, filename):
    # set output to go to file
    with open(filename, 'w') as f:
        f.write("{")
        for i in range(len(skus)):
            # this is not working make this work ####################################
            iteratedSku = json_data['products'][skus[i]]['attributes']
#            if(json_data['products'][skus[i]]['attributes']['servicecode'] == "AmazonEC2"):
            if(iteratedSku['servicecode'] == "AmazonEC2"):
                f.write("\n" + json.dumps(json_data['products'][skus[i]]['sku']) + ' :\n')
                f.write(json.dumps(iteratedSku))
                if i != (len(skus)-1):
                    f.write(",")
        f.write("}")

def main():
    # either grabs default json object or loads from parameters
    data_source = pull_data_source()
    # loads json file determined in pull_data_source
    json_data = set_json(data_source)
    # takes json object and gathers all skus => plus some additional data which will likely be removed
    skus = print_output(json_data)
    # gets all keys from first sku object attributes
    keys = get_keys(json_data['products'][skus[0]]['attributes'])
    for i in range(len(json_data['products'])):
        keys = get_keys(json_data['products'][skus[i]]['attributes'], keys)
    """
    for i in range(len(skus)):
        print('"'+ json_data['products'][skus[i]]['sku'] + '" : {')
        create_json(json_data['products'][skus[i]]['attributes'])
        print("}")
    """
    delete_file_if_exists('test.json')
    output_json(skus, json_data, 'test.json')

if __name__ == '__main__':
    main()
