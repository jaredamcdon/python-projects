#!/usr/bin/env python3

# imports
import json

def pull_file(config_file):
    try:
        with open(config_file, 'r') as data:
            config = data.read()
            return(config)
    except:
        print("Error opening config file\nCheck for {}".format(config_file))
        quit()

def create_conf_obj(config):
    obj = json.loads(config)
    return(obj)


def main():
    conf_json = pull_file('../config.json')
    conf_obj = create_conf_obj(conf_json)
    print(conf_obj)

if __name__ == '__main__':
    main()
