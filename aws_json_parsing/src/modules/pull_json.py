#!/usr/bin/env python3

# IMPORTS IMPORTS
import requests
import os

# use requests library to pull AWS API
def get_json(url):
    request = requests.get(url)
    return(request.json())

# keep a copy of the most recent json pull
def save_old(file_name):
    if(os.path.isfile(file_name)):
        print(file_name)
        old_save = file_name + ".old"
        print(old_save)
        os.rename(file_name, old_save)

# take new json object from API call and save it
def save_json(json_obj, file_name):
    with open(file_name, 'w') as f:
        f.write(json_obj)

def main():
    #json_obj = get_json()
    save_old('test1.json')

if __name__ == '__main__':
    main()

