#!/usr/bin/env python3

# ---IMPORT MODULES--- #

# transform config file to useable data
import modules.get_config as get_config
# get the new index.json from aws
import modules.pull_json as pull_json
# transform aws json
import modules.data_parsing as data_parsing



def main():
    # configure settings with get_config
    conf_json = get_config.pull_file('config.json')
    conf_obj = get_config.create_conf_obj(conf_json)
    # this is just some object testing
    for key in conf_obj:
        print(key)
    
    # pull aws json object, save old with pull_json
    '''pull_json.save_old('data/MOCK_DATA.json')
    '''

    #transform data with data_parsing

if __name__ == '__main__':
    main()
