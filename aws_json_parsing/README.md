# AWS Json Editor
> Looks at all instances of index.json from amazon to compile the best way to analyze data

## Repository Structure

|   location   |  usage  |
|     ---      |   ---   |
| /README.md   | intro, examples, documentation |
| /notes.md    | notepad to document works in progress |
| /config.json | configuration for program |
| /main.py     | python code that handles execution of modules |
|              |   |
| /data/       | stores json data from AWS to be transformed |
|              |   |
| /modules/     | stores individual python sub-programs |
| /modules/data_parsing.py | handles data parsing on AWS json object |
| /modules/pull_json.py | makes API call to pull new json from AWS |
| /modules/get_config.py | pull configuration from config.json |
