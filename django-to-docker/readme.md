# Lets move this app to docker
> After we move the db from sqlite to full fat mysql

## migrate the database

1. `python manage.py dumpdata > dumpdata.json`
2. Update settings.py to my sql
  - 
3. verify connection to db
4. `python manage.py migrate --run-syncdb`
5. exclude contenttype data with this snippet:
  - `python manage.py shell`
    -  `from django.contrib.contenttypes.models import ContentType`
    - `ContentType.objects.all().delete()`
    - `quit()`
6. `python manage.py loaddata datadump.json`
