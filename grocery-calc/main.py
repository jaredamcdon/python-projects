from datetime import date
import os.path
import json

today = date.today().isoformat()

exists = os.path.exists("data.json")
    #json.load(data.json)
if exists:
    pass
else:
    json = open ("data.json", "w")
    json.write('{\n\t"config":\n\t\t{\n\t\t\t"gpd":""\n\t\t},\n\t"lastLogin":\n\t\t{\n\t\t\t"'+ today  +'":"0"\n\t\t}\n}\n')

json_file = open("data.json")
json_data = json.load(json_file)

if json_data["config"]["gpd"] == "":
    print("How much do you want to spend on groceries per day?")
    gpd = input()
    json_data["config"]["gpd"] = gpd

print(json_data["config"]["gpd"])

print(f'today: {today}')

json_file.close()
