import random
print("Welcome to the Slot Machine. You'll start with $10000.")
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

firstWheel = None
win = 0
secondWheel = None
thirdWheel = None
balance = 10000

def main():
    global balance, firstWheel, secondWheel, thirdWheel
    playQuestion = askPlayer()
    while(balance != 0 and playQuestion[0] == True):
        firstWheel = spinWheel()
        secondWheel = spinWheel()
        thirdWheel = spinWheel()
        printScore(playQuestion[1])
        playQuestion = askPlayer()

def askPlayer():
    global balance
    while(True):
      if (balance <=1):
        print ("Machine balance reset.")
        balance = 10000
        
    print("Your balance is " + str(balance))
    answer = input("Would you like to play?")
    if(answer == "yes" or answer == "y"):
        wager = askWager()
        return [True, wager]
    elif(answer == "no" or answer == "n"):
        print("You ended the game with $" + str(balance) + " in your hand. Great job!")
        return False
    else:
        print("Whoops! Didn't get that.")

askWager()
    wager = input("How much would you like to wager?")
    return int(wager)




def spinWheel():
    randomNumber = random.randint(0, 9)
    return numbers[randomNumber]

def printScore(wager):
    global firstWheel, secondWheel, thirdWheel, balance, win
    if((firstWheel == 0) and (secondWheel != 0)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 0) and (secondWheel == 0) and (thirdWheel != 0)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 0) and (secondWheel == 0) and (thirdWheel == 0)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 1) and (secondWheel != 1)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 1) and (secondWheel == 1) and (thirdWheel != 1)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 1) and (secondWheel == 1) and (thirdWheel == 1)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 2) and (secondWheel != 2)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 2) and (secondWheel == 2) and (thirdWheel != 2)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 2) and (secondWheel == 2) and (thirdWheel == 2)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 3) and (secondWheel != 3)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 3) and (secondWheel == 3) and (thirdWheel != 3)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 3) and (secondWheel == 3) and (thirdWheel == 3)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 4) and (secondWheel != 4)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 4) and (secondWheel == 4) and (thirdWheel != 4)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 4) and (secondWheel == 4) and (thirdWheel == 4)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 5) and (secondWheel != 5)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 6) and (secondWheel == 6) and (thirdWheel != 6)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 6) and (secondWheel == 6) and (thirdWheel == 6)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 7) and (secondWheel != 7)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 7) and (secondWheel == 7) and (thirdWheel != 7)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 7) and (secondWheel == 7) and (thirdWheel == 7)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 8) and (secondWheel != 8)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 8) and (secondWheel == 8) and (thirdWheel != 8)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 8) and (secondWheel == 8) and (thirdWheel == 8)):
        win = 2000
        balance = balance + 2000
    elif((firstWheel == 9) and (secondWheel != 9)):
        win = 1000
        balance = balance - 1000
    elif((firstWheel == 9) and (secondWheel == 9) and (thirdWheel != 9)):
        win = 1000
        balance = balance + 1000
    elif((firstWheel == 9) and (secondWheel == 9) and (thirdWheel == 9)):
        win = 2000
        balance = balance + 2000
    else:
      win = 1000
      balance = balance - 1000
    if(win > 0):
        print(firstWheel + "/" + secondWheel + "/" + thirdWheel + " -- You win $" + str(win))
    else:
        print(firstWheel + "/" + secondWheel + "/" + thirdWheel + " -- You lose $" + str(win))

if __name__ == "__main__":
  main()