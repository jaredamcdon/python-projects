from app import app 
from flask_frozen import Freezer

FREEZER_DESTINATION = "./build"
FREEZER_IGNORE_MIMETYPE_WARNINGS = True
FREEZER_SKIP_EXISTING = True
FREEZER_REMOVE_EXTRA_FILES = True

#app.config.from_object(__name__)

#freezer = Freezer(app, with_static_files=True, log_url_for=False, with_no_argument_rules=False)
freezer = Freezer(app)

@freezer.register_generator
def url_generator():
    yield '/'
    yield '/about/'

#@freezer.register_generator
#def venues_generator():
#    from app.models import Venue
#
#    items = Venue.query(Venue.is_active == True)

#    for _item in items:
#            yield 'venue.show', {'url_name': _item.url_name}

if __name__ == '__main__':
    freezer.freeze()
