import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

products = [
        {
            'name':'pencil',
            'price': 5
        },
        {
            'name':'pen',
            'price': 10
        },
        {
            'name':'book',
            'price': 20
        }
]




@app.route('/', methods=['GET'])
def home():
    return "<h1>This is an API testing page.</h1>"

@app.route('/about/',methods=['GET'])
def api_all():
    return jsonify(products)

@app.route('/api',methods=['GET'])
def api_fetch():
    if 'name' in request.args:
        name = request.args['name']
    else:
        return("Error - please specify name")

    results = []

    for product in products:
        if product['name'] == name:
            results.append(product)
    return jsonify(results)

if __name__ == '__main__':
    app.run()
