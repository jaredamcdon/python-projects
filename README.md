# Python project repository
>holds all projects I work on with a .py

## Projects

---

### Amoritization

Calculate investment growth and (I think) non-functioning tax calculator

### aws_json_parsing

work project for trying to manage an automated design for collecting and transcribing json data to a manageable web interface

### binaryCreation

A non-working way of generating infinite list of numbers, maybe one day

### bucketlist-flask-mysql

A first attempt at dockerizing an application, cant get the database to properly connect but doesnt bother me as I run a database farm that I prefer to use anyway. Application built on flask backend with frontend server by python and styling from bootstrap 4.

### classes-and-objects

reference material for working with classes and objects in python. Some objects intentionally not functioning for examples of right/wrong ways of object creation.

### db_parse

Works with .db (sqlite) files to parse, convert to excel, and gives command line tool to modify data

* Dependencies:
    * sqlite3
        * included with python 3.*
    * argparse
        * ''
    * csv
        * ''

### django-learning

Learning django, I don't like how django's tooling requires django-specific knowledge and reminds me of RoR, but it's fine if you have to use it.

### django-to-docker

Currently unsuccessful project that was abandoned due to not liking django. Check [bucketlist-flask-mysql](#bucketlist-flask-mysql) for successful dockerization with python

### firstApi

Rudimentary API not connected to database

* Dependencies:
    * flask

### flaskApp

Basic flask app that connects static files and templating; started at Rolls to learn flask

* Dependencies
    * flask

### helpBlake

Garbage code for garbage project blake needed help with

### json_parsing

some rudimentary scripting for testing how json and argparse function for work later done on [aws-json-parsing](#aws_json_parsing)

### infiniteLetters

Non-functioning tool to create infinite list of strings, similar concept to binaryCreation.

### lab05

quick python written to work with data from a lab in CIT 32700 Wireless Communication

### notebooks

storage for jupyter notebooks. Currently holds api-notebook which is a simple notebook for testing api calls with a python build script that is OS agnostic using os.system to make bash or CMD commands to build out a pipenv to run the application.

### stock-analytics

Application designed for monitoring crypto and traditional markets using async api calls and a flask backend connected to a jam stack with goals for visualization and statistical analysis handled on frontend and backend respectively.

### static-dynamic-api

New project to attempt to create an API that is partially a static site generator and partially a standard API to increase efficiency and lower cost for JAM stacks.

### systemprac

Playing around with using os calls in python

### uvicorn

uvicorn project thats an API?

* Dependencies:
    * uvicorn