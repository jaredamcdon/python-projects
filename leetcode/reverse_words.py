"""
Runtime: 28 ms
Memory Usage: 15 MB
"""

class Solution:
    def reverseWords(self, s: str) -> str:
        out = ""
        # create array based on words in s
		split = s.split(" ")
        for i in split:
            # append out with reversed word via [::-1], add space to end
			out += ("{} ".format(i[::-1]))
        # remove space at end
		out = out[:-1]
        return(out)
