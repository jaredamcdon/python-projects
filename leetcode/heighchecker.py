"""
Runtime: 24 ms, faster than 99.18% of Python3 online submissions
Memory Usage: 14.3 MB, less than 44.30% of Python3 online submissions
###
A school is trying to take an annual photo of all the students. The students are asked to stand in a single file line in non-decreasing order by height. Let this ordering be represented by the integer array expected where expected[i] is the expected height of the ith student in line.

You are given an integer array heights representing the current order that the students are standing in. Each heights[i] is the height of the ith student in line (0-indexed).

Return the number of indices where heights[i] != expected[i].
"""

class Solution:
    def heightChecker(self, heights: List[int]) -> int:
        sort = sorted(heights)
        err = 0
        for i in range(len(heights)):
            if heights[i] != sort[i]:
                err += 1
        return(err)
