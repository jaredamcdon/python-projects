'''
Runtime: 76 ms, faster than 77.38% of Python3 online submissions
Memory Usage: 14.6 MB, less than 5.73% of Python3 online submissions
'''

class Solution:
    def canBeEqual(self, target: List[int], arr: List[int]) -> bool:
        bool = sorted(target) == sorted(arr)
        return(bool)
