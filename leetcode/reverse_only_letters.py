'''
Runtime: 28 ms, faster than 80.39% of Python3 online submissions
Memory Usage: 14.3 MB, less than 48.34% of Python3 online submissions
/---/
Given a string s, return the "reversed" string where all characters that are not a letter stay in the same place, and all letters reverse their positions.
'''
class Solution:
    def reverseOnlyLetters(self, s: str) -> str:
        cont = '-{}`<>.,@#$%^&*()+/[]_;:"!?=1234567890'
        cont+= "'"
        split = s
        for c in cont:
            split = split.replace(c, '')
        split = list(split)
        out =""
        for i in range(len(s)):
            if s[i] in cont:
                out+=s[i]
            else:
                out+=split.pop()
        return(out)
