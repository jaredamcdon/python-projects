"""
Runtime: 60 ms, faster than 61.66% of Python3 online submissions
Memory Usage: 14.4 MB, less than 50.07% of Python3 online submissions
/---/
Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].
Return the array in the form [x1,y1,x2,y2,...,xn,yn]
"""
class Solution:
    def shuffle(self, nums: List[int], n: int) -> List[int]:
        out = []
        for i in range(int(len(nums)/2)):
            out.append(nums[i])
            out.append(nums[i+n])
        return(out)
