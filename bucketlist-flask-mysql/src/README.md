## Initial setup
 - you NEED pipenv to properly manage the packages
 - after installing pipenv run:
   - `pipenv install`
## Basic commands
 - `pipenv run python3 FlaskApp/app.py`
   - do this to activate the pipenv when running the app

## casual links
- [Creating a Web App From Scratch Using Python Flask and MySQL](https://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-22972)
- part 2 at top of page