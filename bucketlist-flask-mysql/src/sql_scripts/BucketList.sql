DROP DATABASE IF EXISTS BucketList;
CREATE DATABASE IF NOT EXISTS BucketList;

USE BucketList;

CREATE TABLE `BucketList`.`tbl_user` (
    `user_id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_name` VARCHAR(45) NULL,
    `user_username` VARCHAR(45) NULL,
    `user_password` VARCHAR(128) NULL,
    PRIMARY KEY (`user_id`)
);

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createUser`(
    IN p_name VARCHAR(20),
    IN p_username VARCHAR(20),
    IN p_password VARCHAR(128)
)
BEGIN
    IF
        ( SELECT EXISTS ( SELECT 1 FROM tbl_user WHERE user_username = p_username ) )
    THEN
        SELECT 'Username Exists !!';
    ELSE
        INSERT INTO tbl_user(user_name, user_username, user_password)
        VALUES (p_name, p_username, p_password);
    END IF;
END$$
DELIMITER ;