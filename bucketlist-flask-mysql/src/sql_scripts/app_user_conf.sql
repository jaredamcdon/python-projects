CREATE USER 'BucketList_app_acc'@'localhost' IDENTIFIED BY 'Buck3tL15t';
GRANT ALL PRIVILEGES ON BucketList.* TO 'BucketList_app_acc'@'localhost';
FLUSH PRIVILEGES;
REVOKE GRANT OPTION ON BucketList.* FROM 'BucketList_app_acc'@'localhost';
FLUSH PRIVILEGES;

CREATE USER 'BucketList_app_acc'@'%' IDENTIFIED BY 'Buck3tL15t';
GRANT ALL PRIVILEGES ON BucketList.* TO 'BucketList_app_acc'@'%';
FLUSH PRIVILEGES;
REVOKE GRANT OPTION ON BucketList.* FROM 'BucketList_app_acc'@'%';
FLUSH PRIVILEGES;
