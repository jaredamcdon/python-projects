# tell django where tf to grab
from django.urls import path

# pull in the data from views
from . import views

app_name = 'polls'
urlpatterns = [
        # i think int:pk helps with generic Views - see views.py
        # ex: polls/
        path('', views.IndexView.as_view(), name='index'),
        # ex: polls/5
        path('<int:pk>/', views.DetailView.as_view(), name='detail'),
        # ex: polls/5/results
        path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
        # ex: polls/5/vote
        path('<int:question_id>/vote/', views.vote, name='vote'),
    ]

""" /*old code when functions were set as index, detail, etc => just calling fn's
    also notice old code uses <int:question_id> instead of pk, just different calls baby */
urlpatterns = [
        # ex: polls/
        path('', views.index, name='index'),
        # ex: polls/5
        path('<int:question_id>/', views.detail, name='detail'),
        # ex: polls/5/results
        path('<int:question_id>/results/', views.results, name='results'),
        # ex: polls/5/vote
        path('<int:question_id>/vote/', views.vote, name='vote'),
    ]
"""