from django.contrib import admin

# Register your models here.

# make question object modifiable from http.../admin
from .models import Question
admin.site.register(Question)