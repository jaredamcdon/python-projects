# for 404s and obv rendering
from django.shortcuts import render, get_object_or_404

# for 404s, else idk
from django.http import HttpResponse, HttpResponseRedirect, Http404

# template importing 
from django.template import loader

# not sure what reverse does exactly - used in vote - some kind of POST function
from django.urls import reverse

# pulls defaults for views - this helps us set up templating
# i.e. - /detail.html pulls from /detail_detail.html like pug and mixins
from django.views import generic

# Create your views here.

# pulls data from models.py
from .models import Choice, Question

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        # return the last five published questions
        return Question.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    # template_name overrides defaults - as a DetailView with the 
    # model = question => default filename = question_detail.html
   # template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

def vote(request, question_id):
    # /* old code*/ return HttpResponse("You're voting on question %s." % question_id)
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except:
        # rediplay question voting form
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a 
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


""" /* old code - see urls.py */
def index(request):
    # /*original code*/ return HttpResponse("Hello world. You're at the polls index.")
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list' : latest_question_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'polls/detail.html', {'question': question})
    # /*old code*/ return HttpResponse("You're looking at the results of question %s." % question_id)

def results(request, question_id):
    # /*old code*/ response = "You're looking at the results of question %s."
    # /*old code*/ return HttpResponse(response % question_id)
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

"""