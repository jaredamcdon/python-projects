## DJango Notes
> All information from [django's documentation](docs.djangoproject.com/en)

### Commands table

| Command Syntax | Command Usage | Useful flags | Notes |
| 	    --- 	| 	    --- 	 |      ---     |  ---  |
| `django-admin startproject [projectName]` | Auto configuration of server | | |
| `python3 manage.py runserver` | starts webserver | 0.0.0.0:8000 (change port/addr) | |
| `python3 manage.py startapp [polls]` | build [polls] app | | |
| `python manage.py makemigrations [polls]` | add 'app' to config | | must first add [polls] to settings.py.INSTALLED_APPS |
| `python manage.py sqlmigrate [polls 0001]` | add db config to app | | `polls 0001` pulled from CLI output & polls/migrations/0001_initial.py - run `python manage.py migrate` after|
| `python manage.py check` | checks for problems without modifications | | |
| `python manage.py shell` | API/shell access | | |
| `python manage.py createsuperuser` | Django admin account creation| | this app uses admin, admin, admin@admin.com |

### Code structure
| location | Usage |
|    ---   |  ---  |
| [projName]/ | Root of [projName] |
| /manage.py | CLI ustility to interact with the project | 
| /[projName]/ * | python package for the project |
| * /\_\_init__.py | empty file to tell python this is a package |
| * /settings.py | settings/config for the Django project |
| * /urls.py | router-esque |
| * /asgi.py | entry point for asgi-web servers |
| * /wsgi.py | entry point for wsgi-web servers |

