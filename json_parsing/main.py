#!/usr/bin/env python3

# import packages dummy
import json
import argparse

# import the data
parser = argparse.ArgumentParser()


parser.add_argument('-d', '--data', nargs='*')
args = parser.parse_args()

try:
    args.data
except:
    data_source = 'data/MOCK_DATA.json'
else:
    data_source = args.data[0]
print(data_source)

try:
    with open(data_source) as json_file:
        data = json.load(json_file)
except:
    print('An error occured loading json. Check for: {}'.format(data_source))
    quit()

#print(data)
print('\n=====   ///   =====\n')
print(data[-1])

keys = list(data[0].keys())

print(keys)

print(keys[0])

print(data[-1]['id'])
print(data[-1][keys[0]])
print(data[0]['id'])
