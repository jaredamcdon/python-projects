total = income = int(input('salary:'))
def taxFunc(income, threshold, percentage, alreadyPaid):
    amtTaxable = income - threshold
    if(amtTaxable <= 0):
        amtTaxable = income
    taxPaid = (amtTaxable * (percentage / 100)) + alreadyPaid
    income = income - threshold
    return (income, taxPaid)

if (total >= 518401):
     result = taxFunc(income, 518401, 37, 0)
if (total >= 207351):
    if ('result' in globals()):
        result = taxFunc(result[0], 207351,35, result[1])
    else:
        result = taxFunc(income, 207351,35,0)
if (total >= 163301):
    if ('result' in globals()):
        result = taxFunc(result[0],163301,32, result[1])
    else:
        result = taxFunc(income, 163301,32,0)
if (total >= 85526):
    if ('result' in globals()):
        result = taxFunc(result[0], 85526,24, result[1])
    else:
        result = taxFunc(income, 85526,24,0)
if (total >= 40126):
    if ('result' in globals()):
        result = taxFunc(result[0], 40126,22, result[1])
    else:
        result = taxFunc(income, 40126,22,0)
if ('result' in globals()):
    result = taxFunc(result[0], 9875,10, result[1])
else:
    result = taxFunc(income, 9875,10,0)
print('Tax Paid: $' + str(result[1]) + '\tNet Income: $' + str(total - result[1]))
