total = amt = int(input('$/yr: '))
print("-----------------------------------------\n|\tYear\t|\tAmount\t\t|\n|---------------|-----------------------|")
for i in range(1,26):
    if (i == 1):
        total = total
    else:
        total = (total*1.08) + amt
    if (i % 5 == 0):
        print("|\t{}\t|\t${}\t|".format(i, round(total, 2)))
print("-----------------------------------------")
