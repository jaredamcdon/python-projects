
# make constants all caps
PI = 3.14159

# assign two vars at once
minValue, maxValue = 1, 100

# returns greater
print("greater"[:])

# returns eater
print("greater"[2:])

# returns gr
print("greater"[:2])

# returns eat
print("greater"[2:5])


#--- some interesting formatting stuff

# right justify string in six spaces
print("%6s" % "four")

# left justify string in six spaces
print("%-6s" % "four")

# right justify integer in eight spaces
print("%8d" % 1234)

#justify between 3 and 12
for exponent in range(7,11):
    print("%-3d%12d" % (exponent, 10**exponent))

#--- precise decimal formatting

# print with 2 decimal places
print("%0.2f" % PI)

# print with a width of 8 and 3 decimals
print("%6.3f" % PI)

#--- str manipulation

# returns false
print("greater".isupper())

# prints GREATER
print("greater".upper())

# prints true
print("greater".startswith("great"))

# other ways of calling shit
print("con" + "cat")

print("con".__add__("cat"))


