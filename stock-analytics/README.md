## Stock Analysis
> Using free APIs to find my best correlations while comparing to junk data

## Current Important implementations/notes
- with python requests it needs data validations for json
	- i.e. the data for realstonks is json but is parsed as a string
	- code block:
```python
if isinstance(data, str):
	data = json.loads(data)

# or // not sure which to use - both seem to work - isinstance is inheritance based

if type(data) == str:
	data = json.loads(data)

#or

if type != list or type != dict:
	data = json.loads(data)
```


## What I Want to Add

- plane ticket prices
- sports information
- hotel pricing 
- sold/listed houses

## Plans
 - python scripts for data collection
	- Done via API calls at set intervals asynchronously and schedule-based using something like python celery 
 - Front end?
	- JAM stack? 
		- host through netlify/locally on stocks.jdogg.club or something
    - either vue or react depending on how much I like react
	- maybe write in typescript to learn typescript
 - Back end
 	- Python back-end
	- thinking about making all calls through ajax requests
		- Connect frontend to jdogg.club with python router handling requests
 - Data storage
	- MySQL/Maria
 - Analysis plans
	- Visualization on frontend with whatever [Data Visualization With JavaScript](https://nostarch.com/datavisualization) recommends or d3.js
	- backend analysis with whatever [Data Science from Scratch](https://www.oreilly.com/library/view/data-science-from/9781491901410/) or [Doing Math with Python](https://nostarch.com/doingmathwithpython) says probably
 - Unit testing
	- dear lord lets work in unit testing please?
 - containerization
	- probably dockerize the backend to make CI/CD not a nightmare
 - api keys
	- maybe find a way to hide them from the git repo?

## Current progress
- [x] metadata database
- [x] data database
- [x] synchronous, programmatic python data ingest/regest script
- [x] asynchronous automated api calling
- [x] web backend
- [ ] web frontend
- [x] api for frontend
- [ ] data analysis
- [ ] automated data analysis
- [ ] data science
- [ ] automated data science

## Database design
- [database diagram](https://app.quickdatabasediagrams.com/#/d/CxEYlL)
- markup:
```
api
---
ID INT PK
name VARCHAR
url VARCHAR
category VARCHAR
dayLimit INTEGER
monthLimit INTEGER
processing BOOLEAN

args
---
ID INT PK
pKey VARCHAR
pValue VARCHAR
header BOOLEAN
param BOOLEAN

api_args
---
ID INT PK
apiID INT FK - api.ID
paramsID INT FK >- args.ID

cols
---
ID INT PK FK -< tables_cols.colsID
col VARCHAR
dataType VARCHAR
size VARCHAR

tables
---
ID INT PK
name VARCHAR
apiID INT FK >- api.ID

tables_cols
---
ID INT PK
tablesID INT FK >- tables.ID
colsID INT FK

vars
---
ID INT PK
keyword VARCHAR(64)
fn VARCHAR(32)
tables_colsID INT FK - tables_cols.ID

response
---
ID INT PK
apiID INT FK >- api.ID
varsID INT FK -< vars.ID
headerID INT FK -< header.ID

header
---
ID INT PK
headerCode INT
isTrue BOOLEAN
tables_colsID INT FK - tables_cols.ID

```
