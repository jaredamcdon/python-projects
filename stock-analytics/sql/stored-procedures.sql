USE stonkAPI;

DROP PROCEDURE IF EXISTS getResponse;
DROP PROCEDURE IF EXISTS fetchApiData;

DELIMITER $$
CREATE PROCEDURE getResponse
(IN tmp_api_ID INT(4))

BEGIN
    SET @isHeader := 
    (SELECT response.headerID FROM response
    INNER JOIN api ON api.ID = response.apiID
    WHERE api.ID = tmp_api_id
    LIMIT 1
    );
    IF @isHeader IS NULL THEN
        SELECT tables.name AS table_name, cols.col, vars.keyword, vars.fn FROM response
            INNER JOIN vars ON vars.ID = response.varsID
            INNER JOIN tables_cols ON tables_cols.ID = vars.tables_colsID
            INNER JOIN tables ON tables.id = tables_cols.tablesID
            INNER JOIN cols ON cols.ID = tables_cols.colsID
            INNER JOIN api ON api.ID = tables.apiID
        WHERE api.ID = tmp_api_ID;
	ELSE
        SELECT tables.name AS table_name, cols.col, header.headerCode, header.isTrue FROM response
            INNER JOIN header ON header.ID = response.headerID
            INNER JOIN tables_cols ON tables_cols.ID = header.tables_colsID
            INNER JOIN tables ON tables.id = tables_cols.tablesID
            INNER JOIN cols ON cols.ID = tables_cols.colsID
            INNER JOIN api ON api.ID = tables.apiID
        WHERE api.ID = tmp_api_ID;
    END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE fetchApiData
(IN tmp_api_id INT(4))

BEGIN
    SELECT args.pKey, args.pValue, args.header, args.param FROM args
        INNER JOIN api_args on args.ID = api_args.paramsID
        INNER JOIN api ON api_args.apiID = api.ID
        WHERE api.ID = tmp_api_id;
END$$
DELIMITER ;