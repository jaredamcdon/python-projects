
use stonkAPI;

#get url and all headers/params for all apis
SELECT api.name, api.url, args.pKey, args.pValue FROM api
INNER JOIN api_args ON api_args.apiID = api.ID
INNER JOIN args ON api_args.paramsID = args.ID
#GROUP BY api.ID
;

#get last api ID
SELECT api.ID from api
ORDER BY api.ID DESC
LIMIT 1;

#get url and all headers/params for an api
SELECT api.name, api.url, args.pKey, args.pValue FROM api
INNER JOIN api_args ON api_args.apiID = api.ID
INNER JOIN args ON api_args.paramsID = args.ID
WHERE api.ID = 1001;

#get all columns for all tables
SELECT tables.name, cols.col, cols.dataType, cols.size FROM tables
INNER JOIN tables_cols ON tables.ID = tables_cols.tablesID
INNER JOIN cols ON tables_cols.colsID = cols.ID;

#get all columns for a table based on api
SELECT tables.name, cols.col, cols.dataType, cols.size FROM tables
INNER JOIN tables_cols ON tables.ID = tables_cols.tablesID
INNER JOIN cols ON tables_cols.colsID = cols.ID
INNER JOIN api ON tables.apiID = api.ID
WHERE api.ID = 1000;


#get api calling information
SELECT api.url, args.pKey, args.pValue, args.header, args.param FROM api
INNER JOIN api_args ON api_args.apiID = api.ID
INNER JOIN args ON args.ID = api_args.paramsID
WHERE api.ID = 1001;

#get all response variables for a given table
SELECT tables.name AS table_name, cols.col, vars.keyword, vars.fn FROM response
INNER JOIN vars ON vars.ID = response.varsID
INNER JOIN tables_cols ON tables_cols.ID = vars.tables_colsID
INNER JOIN tables ON tables.id = tables_cols.tablesID
INNER JOIN cols ON cols.ID = tables_cols.colsID
INNER JOIN api ON api.ID = tables.apiID
#INNER JOIN header ON header.ID = response.headerID
WHERE api.ID = 1001;

/*
 ____             _____        _
|  _ \  _____   _|_   ____ ___| |_
| | | |/ _ \ \ /   | |/ _ / __| __|
| |_| |  __/\ V /  | |  __\__ | |_
|____/ \___| \_/   |_|\___|___/\__|
*/

SELECT api.name, api.id FROM api;

#stored procedure => this has if/else for header/var response
CALL getResponse(1001);
SELECT @isHeader;
#this getsapi calling information
CALL fetchApiData(1001);


### data aggregation testing

use stonkData;

show tables;

SELECT * FROM weather_chi WHERE date_time >= NOW() - INTERVAL 1 DAY;

DESCRIBE weather_chi;