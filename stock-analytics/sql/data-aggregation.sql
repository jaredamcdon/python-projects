DROP DATABASE IF EXISTS stonkShorts;

CREATE DATABASE stonkShorts;
USE stonkShorts;

DROP TABLE IF EXISTS stonkShorts;
/*
CREATE TABLE aggregate3(
ID			INTEGER(6) NOT NULL AUTO_INCREMENT,
date_time	DATETIME DEFAULT NOW(),
avg_total	INTEGER(4),
last24		BOOLEAN DEFAULT TRUE,
PRIMARY KEY	(ID)
);

CREATE TABLE aggregate24(
ID			INTEGER(6) NOT NULL AUTO_INCREMENT,
date_time	DATETIME DEFAULT NOW(),
avg_total	INTEGER(4),
last7		BOOLEAN DEFAULT TRUE,
PRIMARY KEY	(ID)
);

#i need to choose a table design - these are my current ideas?
*/

show tables;

describe aggregate3;
describe aggregate24;
