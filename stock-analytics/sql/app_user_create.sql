DROP USER IF EXISTS 'stonk_app_acc'@'localhost';
DROP USER IF EXISTS 'stonk_app_acc'@'%';

CREATE USER 'stonk_app_acc'@'localhost' IDENTIFIED BY '1337_5t0nk5';

GRANT ALL PRIVILEGES ON stonkAPI.* TO 'stonk_app_acc'@'localhost';
GRANT ALL PRIVILEGES ON stonkData.* TO 'stonk_app_acc'@'localhost';
GRANT ALL PRIVILEGES ON stonkShorts.* TO 'stonk_app_acc'@'localhost';
FLUSH PRIVILEGES;
REVOKE GRANT OPTION ON stonkAPI.* FROM 'stonk_app_acc'@'localhost';
REVOKE GRANT OPTION ON stonkData.* FROM 'stonk_app_acc'@'localhost';
REVOKE GRANT OPTION ON stonkShorts.* FROM 'stonk_app_acc'@'localhost';
FLUSH PRIVILEGES;

CREATE USER 'stonk_app_acc'@'%' IDENTIFIED BY '1337_5t0nk5';

GRANT ALL PRIVILEGES ON stonkAPI.* TO 'stonk_app_acc'@'%';
GRANT ALL PRIVILEGES ON stonkData.* TO 'stonk_app_acc'@'%';
GRANT ALL PRIVILEGES ON stonkShorts.* TO 'stonk_app_acc'@'%';
FLUSH PRIVILEGES;
REVOKE GRANT OPTION ON stonkAPI.* FROM 'stonk_app_acc'@'%';
REVOKE GRANT OPTION ON stonkData.* FROM 'stonk_app_acc'@'%';
REVOKE GRANT OPTION ON stonkShorts.* FROM 'stonk_app_acc'@'%';
FLUSH PRIVILEGES;