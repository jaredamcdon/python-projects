DROP DATABASE IF EXISTS stonkData;

CREATE DATABASE IF NOT EXISTS stonkData;
USE stonkData;

CREATE TABLE adsbx(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    total       INTEGER(4),
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_ada(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_btc(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_doge(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_eth(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_ltc(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_uni(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coingecko_xmr(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(8,2) NOT NULL,
    mkt_cap     DECIMAL(18,5) NOT NULL,
    24hr_vol    DECIMAL(18,5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE coinpaprika(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    mkt_cap     BIGINT(24) NOT NULL,
    24hr_vol    BIGINT(24) NOT NULL,
    btc_dom     DECIMAL(4,2) NOT NULL,
    crypt_num   INTEGER(5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE nytimes(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    world_cnt   INTEGER(2),
    biz_cnt     INTEGER(2),
    climate_cnt INTEGER(2),
    ny_cnt      INTEGER(2),
    brief_cnt   INTEGER(2),
    pod_cnt     INTEGER(2),
    oped_cnt    INTEGER(2),
    sci_cnt     INTEGER(2),
    arts_cnt    INTEGER(2),
    tech_cnt    INTEGER(2),
    finance_cnt INTEGER(2),
    sport_cnt   INTEGER(2),
    us_cnt      INTEGER(2),
    food_cnt    INTEGER(2),
    well_cnt    INTEGER(2),
    travel_cnt  INTEGER(2),
    style_cnt   INTEGER(2),
    health_cnt  INTEGER(2),
    real_estate INTEGER(2),
    PRIMARY KEY (ID)
);

CREATE TABLE ticketmaster(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    events      INTEGER(3) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE tmdb1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    avg_pop     DECIMAL(6,2) NOT NULL,
    adult_cnt   INTEGER(2) NOT NULL,
    en_cnt      INTEGER(2) NOT NULL,
    es_cnt      INTEGER(2) NOT NULL,
    ja_cnt      INTEGER(2) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE tmdb2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    cnt         INTEGER(5) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE exchange_rates(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    aud         DECIMAL(3,2) NOT NULL,
    cad         DECIMAL(3,2) NOT NULL,
    cny         DECIMAL(3,2) NOT NULL,
    eur         DECIMAL(3,2) NOT NULL,
    gbp         DECIMAL(3,2) NOT NULL,
    hkd         DECIMAL(4,3) NOT NULL,
    jpy         DECIMAL(5,2) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE nager_holiday_today(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    is_holiday  BOOLEAN,
    PRIMARY KEY (ID)
);

CREATE TABLE realtor_ny_hot_score(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    score       DECIMAL(8,6),
    PRIMARY KEY (ID)
);

CREATE TABLE twelve_data_stocks(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    ticker_all  DECIMAL(9,5),
    ticker_aapl DECIMAL(9,5),
    ticker_cvx  DECIMAL(9,5),
    ticker_dis  DECIMAL(9,5),
    ticker_hlt  DECIMAL(9,5),
    ticker_rblx DECIMAL(9,5),
    ticker_tlt  DECIMAL(9,5),
    ticker_vti  DECIMAL(9,5),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_tech_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_financial_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_financial_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_consumer_leis_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_consumer_leis_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_consumer_staples_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_consumer_staples_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_util_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_util_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_healthcare_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_gold_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_silver_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_copper_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_nat_resources_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_nat_resources_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_nrg_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_nrg_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_transport_ind1(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_transport_ind2(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE realstonks_treasury_long_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    price       DECIMAL(6,2),
    change_pt   DECIMAL(5,2),
    change_pct  DECIMAL(4,2),
    volume      INT(16),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_ind(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_hollywood(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_ny(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_sanfran(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_denver(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_miami(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_dc(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_minneapolis(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_houston(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);

CREATE TABLE weather_chi(
    ID          INTEGER(6) NOT NULL AUTO_INCREMENT,
    date_time   DATETIME DEFAULT NOW(),
    temp_f      DECIMAL(4,1),
    cloud       INTEGER(3),
    humidity    INTEGER(3),
    PRIMARY KEY (ID)
);