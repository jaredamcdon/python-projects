# Stock-analytics /sql

## api-storage.sql
- Primary database for storing the how of the APIs.
- Contains all build files for:
    - Holding data on the APIs
    - How the application connects

## app_user_create.sql
- Creates an app user account with somewhat restricted permissions to the database.

## data-storage.sql
- Primary database for storing the data that will be analyzed and served to the frontend.

## test-scripts.sql
- Collection of scripts that test the functionality of the database and store calls that I make programatically use in the backend.