DROP DATABASE IF EXISTS stonkAPI;

CREATE DATABASE IF NOT EXISTS stonkAPI;
USE stonkAPI;

CREATE TABLE api(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
name        VARCHAR(64),
url         VARCHAR(128),
category    VARCHAR(20),
dayLimit    INTEGER(4),
monthLimit  INTEGER(4),
processing	BOOLEAN,
PRIMARY KEY (ID)
);

ALTER TABLE api AUTO_INCREMENT = 1000;

CREATE TABLE args(
ID	        INTEGER(4) NOT NULL AUTO_INCREMENT,
pKey	    VARCHAR(64) NOT NULL,
pValue	    VARCHAR(128) NOT NULL,
header      BOOLEAN,
param       BOOLEAN,
PRIMARY KEY (ID)
);

ALTER TABLE args AUTO_INCREMENT = 2000;

CREATE TABLE api_args(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
apiID       INTEGER(4) NOT NULL REFERENCES api(ID),
paramsID    INTEGER(4) NOT NULL REFERENCES args(ID),
PRIMARY KEY (ID)
);

ALTER TABLE api_args AUTO_INCREMENT = 3000;

CREATE TABLE tables(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
name        VARCHAR(32) NOT NULL,
apiID       INTEGER(4) NOT NULL REFERENCES api(ID),
PRIMARY KEY (ID)
);

ALTER TABLE tables AUTO_INCREMENT = 4000;

CREATE TABLE cols(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
col         VARCHAR(20) NOT NULL,
dataType    VARCHAR(8) NOT NULL,
size        VARCHAR(4),
PRIMARY KEY (ID)
);

ALTER TABLE cols AUTO_INCREMENT = 5000;

CREATE TABLE tables_cols(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
tablesID    INTEGER(4) NOT NULL REFERENCES tables(ID),
colsID      INTEGER(4) NOT NULL REFERENCES cols(ID),
PRIMARY KEY (ID)
);

ALTER TABLE tables_cols AUTO_INCREMENT = 6000;

CREATE TABLE response(
ID          INTEGER(4) NOT NULL AUTO_INCREMENT,
apiID       INTEGER(4) NOT NULL REFERENCES api.ID,
varsID      INTEGER(4) REFERENCES vars.ID,
headerID    INTEGER(4) REFERENCES header.ID,
PRIMARY KEY (ID)
);

ALTER TABLE response AUTO_INCREMENT = 7000;

CREATE TABLE header(
ID              INTEGER(4) NOT NULL AUTO_INCREMENT,
headerCode      INTEGER(3),
isTrue          BOOLEAN,
tables_colsID   INTEGER(4) NOT NULL REFERENCES tables_cols.ID,
PRIMARY KEY (ID)
);

ALTER TABLE header AUTO_INCREMENT = 8000;

CREATE TABLE vars(
ID              INTEGER(4) NOT NULL AUTO_INCREMENT,
keyword         VARCHAR(128),
fn              VARCHAR(32),
tables_colsID   INTEGER(4) NOT NULL REFERENCES tables_cols.ID,
PRIMARY KEY (ID)
);

ALTER TABLE vars AUTO_INCREMENT = 9000;

## data entry

INSERT INTO api(name, url, category, dayLimit, monthLimit, processing) VALUES
('ADSB Exchange', 'https://adsbx-flight-sim-traffic.p.rapidapi.com/api/aircraft/json/lat/33.636667/lon/84.428056/dist/25/', 'Air Traffic', null, 360, TRUE), #1000
('CoinGecko', 'https://api.coingecko.com/api/v3/simple/price', 'Crypto', null, null, FALSE), #1001
('Coinpaprika', 'https://api.coinpaprika.com/v1/global', 'Crypto', null, null, FALSE), #1002
('NYTimes', 'https://api.nytimes.com/svc/topstories/v2/home.json', 'Entertainment', null, null, FALSE), #1003
('Ticketmaster', 'https://app.ticketmaster.com/discovery/v2/events.json', 'Entertainment', 5000, null, FALSE), #1004
('TMDB1', 'https://api.themoviedb.org/3/trending/movie/day', 'Entertainment', null, null, FALSE), #1005
('TMDB2', 'https://api.themoviedb.org/3/movie/changes', 'Entertainment', null, null, FALSE), #1006
('ExchangeRate-Api', 'https://open.er-api.com/v6/latest', 'Exchange Rates', null, null, FALSE), #1007
('Nager.Date', 'https://date.nager.at/api/v3/IsTodayPublicHoliday/us?offset=-4', 'Holidays', null, null, FALSE), #1008
('Realtor API', 'https://realtor.p.rapidapi.com/locations/auto-complete', 'housing', null, 500, FALSE), #1009
('Twelve Data', 'https://twelve-data1.p.rapidapi.com/price', 'stocks', 66, null, FALSE), #1010
('realstonks tech index', 'https://realstonks.p.rapidapi.com/QQQ', 'stocks', null, 4800, TRUE), #1011
('realstonks financial index1', 'https://realstonks.p.rapidapi.com/VFAIX', 'stocks', null, 4800, TRUE), #1012
('realstonks financial index2', 'https://realstonks.p.rapidapi.com/IYF', 'stocks', null, 4800, TRUE), #1013
('realstonks consumer leisure index1', 'https://realstonks.p.rapidapi.com/VCDAX', 'stocks', null, 4800, TRUE), #1014
('realstonks consumer leisure index2', 'https://realstonks.p.rapidapi.com/XRT', 'stocks', null, 4800, TRUE), #1015
('realstonks consumer staples index1', 'https://realstonks.p.rapidapi.com/VCSAX', 'stocks', null, 4800, TRUE), #1016
('realstonks consumer staples index2', 'https://realstonks.p.rapidapi.com/XLP', 'stocks', null, 4800, TRUE), #1017
('realstonks utilities index1', 'https://realstonks.p.rapidapi.com/FIUIX', 'stocks', null, 4800, TRUE), #1018
('realstonks utilities index2', 'https://realstonks.p.rapidapi.com/XLU', 'stocks', null, 4800, TRUE), #1019
('realstonks healthcare index', 'https://realstonks.p.rapidapi.com/VGHCX', 'stocks', null, 4800, TRUE), #1020
('realstonks gold index', 'https://realstonks.p.rapidapi.com/GLD', 'stocks', null, 4800, TRUE), #1021
('realstonks silver index', 'https://realstonks.p.rapidapi.com/SLV', 'stocks', null, 4800, TRUE), #1022
('realstonks copper index', 'https://realstonks.p.rapidapi.com/JJC', 'stocks', null, 4800, TRUE), #1023
('realstonks natural resources index1', 'https://realstonks.p.rapidapi.com/MXI', 'stocks', null, 4800, TRUE), #1024
('realstonks natural resources index2', 'https://realstonks.p.rapidapi.com/VMIAX', 'stocks', null, 4800, TRUE), #1025
('realstonks energy index1', 'https://realstonks.p.rapidapi.com/VENAX', 'stocks', null, 4800, TRUE), #1026
('realstonks energy index2', 'https://realstonks.p.rapidapi.com/XLE', 'stocks', null, 4800, TRUE), #1027
('realstonks transportation index1', 'https://realstonks.p.rapidapi.com/IYT', 'stocks', null, 4800, TRUE), #1028
('realstonks transportation index2', 'https://realstonks.p.rapidapi.com/FSRFX', 'stocks', null, 4800, TRUE), #1029
('realstonks vanguard Long Term Treasury ETF', 'https://realstonks.p.rapidapi.com/VGLT', 'stocks', null, 4800, TRUE), #1030
('Weatherapi.com IND', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1031
('Weatherapi.com Hollywood', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1032
('Weatherapi.com NY', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1033
('Weatherapi.com San Fran', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1034
('Weatherapi.com Denver', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1035
('Weatherapi.com Miami', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1036
('Weatherapi.com DC', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1037
('Weatherapi.com Minneapolis', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1038
('Weatherapi.com Houston', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE), #1039
('Weatherapi.com Chicago', 'https://weatherapi-com.p.rapidapi.com/current.json', 'weather', null, 100000, FALSE) #1040
;

INSERT INTO args(pKey, pValue, header, param) VALUES
('x-rapidapi-key', 'e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d', true, false), #2000
('x-rapidapi-host', 'adsbx-flight-sim-traffic.p.rapidapi.com', true, false), #2001
('ids', 'bitcoin,ethereum,cardano,litecoin,dogecoin,monero,uniswap', false, true), #2002
('vs_currencies', 'usd', false, true), #2003
('include_24hr_vol', 'true', false, true), #2004
('include_market_cap', 'true', false, true), #2005
('', '', false, false), #2006
('api-key','GNLGJGSfDPTxl6ETaEkS7CbQGD6MthQu', false, true), #2007
('apikey','S0TTarBAAKl0L4KuNiU9XjOQoyWlCv3P', false, true), #2008
('postalCode', '90068', false, true), #2009
('api_key', 'c82c49e392039440f9f6e1a7095a87c2', false, true), #2010
('api_key', 'c82c49e392039440f9f6e1a7095a87c2', false, true), #2011
('x-rapidapi-host', 'realtor.p.rapidapi.com', true, false), #2012
('input', 'new-york', false, true), #2013
('x-rapidapi-host','twelve-data1.p.rapidapi.com', true, false), #2014
('symbol', 'all,aapl,cvx,dis,hlt,rblx,tlt,vti', false, true), #2015
('x-rapidapi-host', 'realstonks.p.rapidapi.com', true, false), #2016
('x-rapidapi-host', 'weatherapi-com.p.rapidapi.com', true, false), #2017
('q', '46201', false, true), #2018
('q', '90210', false, true), #2019
('q', '10005', false, true), #2020
('q', '94188', false, true), #2021
('q', '80299', false, true), #2022
('q', '33186', false, true), #2023
('q', '20011', false, true), #2024
('q', '55433', false, true), #2025
('q', '77084', false, true), #2026
('q', '60629', false, true) #2027
;

INSERT INTO api_args(apiID, paramsID) VALUES
(1000,2000), #ADSB, key
(1000,2001), #ADSB, host
(1001,2002), #CoinGecko, ids
(1001,2003), #CoinGecko, vs_currency
(1001,2004), #CoinGecko, 24hr vol
(1001,2005), #CoinGecko, mkt cap
(1002,2006), #Coinpaprika, empty params
(1003,2007), #NYTimes, apikey
(1004,2008), #Ticketmaster, apikey
(1004,2009), #Ticketmaster, location
(1005,2010), #TMDB1, api_key
(1006,2011), #TMDB2, api_key
(1007,2006), #exchange-rate, empty params
(1008,2006), #nager.date, empty params
(1009,2000), #realtor api, key
(1009,2012), #realtor api, host
(1009,2013), #realtor api, input location
(1010,2000), #twelve data, key
(1010,2014), #twelve data, host
(1010,2015), #twelve data, stock names
(1011,2000), #realstonks tech index, key
(1011,2016), #realstonks tech index, host
(1012,2000), #realstonks fin ind1, key
(1012,2016), #realstonks fin ind1, host
(1013,2000), #realstonks fin ind2, key
(1013,2016), #realstonks fin ind2, host
(1014,2000), #realstonks con leis ind1, key
(1014,2016), #realstonks con leis ind1, host
(1015,2000), #realstonks con leis ind2, key
(1015,2016), #realstonks con leis ind2, host
(1016,2000), #realstonks con stap ind1, key
(1016,2016), #realstonks con stap ind1, host
(1017,2000), #realstonks con stap ind2, key
(1017,2016), #realstonks con stap ind2, host
(1018,2000), #realstonks util ind1, key
(1018,2016), #realstonks util ind1, host
(1019,2000), #realstonks util ind2, key
(1019,2016), #realstonks util ind2, host
(1020,2000), #realstonks hca ind, key
(1020,2016), #realstonks hca ind, host
(1021,2000), #realstonks gold ind, key
(1021,2016), #realstonks gold ind, host
(1022,2000), #realstonks silver ind, key
(1022,2016), #realstonks silver ind, host
(1023,2000), #realstonks copper ind, key
(1023,2016), #realstonks copper ind, host
(1024,2000), #realstonks nat res ind1, key
(1024,2016), #realstonks nat res ind1, host
(1025,2000), #realstonks nat res ind2, key
(1025,2016), #realstonks nat res ind2, host
(1026,2000), #realstonks nrg ind1, key
(1026,2016), #realstonks nrg ind1, host
(1027,2000), #realstonks nrg ind2, key
(1027,2016), #realstonks nrg ind2, host
(1028,2000), #realstonks trans ind1, key
(1028,2016), #realstonks trans ind1, host
(1029,2000), #realstonks trans ind2, key
(1029,2016), #realstonks trans ind2, host
(1030,2000), #realstonks treas etf, key
(1030,2016), #realstonks treas etf, host
(1031,2000), #weatherapi IND,key
(1031,2017), #weatherapi IND, host
(1031,2018), #weatherapi IND, zipcode
(1032,2000), #weatherapi hollywood,key
(1032,2017), #weatherapi hollywood, host
(1032,2019), #weatherapi hollywood, zipcode
(1033,2000), #weatherapi NY,key
(1033,2017), #weatherapi NY, host
(1033,2020), #weatherapi NY, zipcode
(1034,2000), #weatherapi San Fran,key
(1034,2017), #weatherapi San Fran, host
(1034,2021), #weatherapi San Fran, zipcode
(1035,2000), #weatherapi Denver,key
(1035,2017), #weatherapi Denver, host
(1035,2022), #weatherapi Denver, zipcode
(1036,2000), #weatherapi Miami,key
(1036,2017), #weatherapi Miami, host
(1036,2023), #weatherapi Miami, zipcode
(1037,2000), #weatherapi DC,key
(1037,2017), #weatherapi DC, host
(1037,2024), #weatherapi DC, zipcode
(1038,2000), #weatherapi Minneap,key
(1038,2017), #weatherapi Minneap, host
(1038,2025), #weatherapi Minneap, zipcode
(1039,2000), #weatherapi Houston,key
(1039,2017), #weatherapi Houston, host
(1039,2026), #weatherapi Houston, zipcode
(1040,2000), #weatherapi Chi,key
(1040,2017), #weatherapi Chi, host
(1040,2027) #weatherapi Chi, zipcode
;

INSERT INTO tables(name, apiID) VALUES
('adsbx',1000), #adsb, 4000
('coingecko_ada',1001), #coingecko, 4001
('coingecko_btc',1001), #coingecko, 4002
('coingecko_doge',1001), #coingecko, 4003
('coingecko_eth',1001), #coingecko, 4004
('coingecko_ltc',1001), #coingecko, 4005
('coingecko_uni',1001), #coingecko, 4006
('coingecko_xmr',1001), #coingecko, 4007
('coinpaprika', 1002), #coinpaprika, 4008
('nytimes',1003), #NYTimes, 4009
('ticketmaster',1004), #Ticketmaster, 4010
('tmdb1',1005), #TMDB1, 4011
('tmdb2',1006), #TMDB2, 4012
('exchange_rates',1007), #ExchangeRate-Api, 4013
('nager_holiday_today',1008), #Nager.Date, 4014
('realtor_ny_hot_score',1009), #Realtor Api, 4015
('twelve_data_stocks',1010), #Twelve Data, 4016
('realstonks_tech_ind',1011), #realstonks tech index, 4017
('realstonks_financial_ind1',1012), #realstonks financial index1, 4018
('realstonks_financial_ind2',1013), #realstonks financial index2, 4019
('realstonks_consumer_leis_ind1',1014), #realstonks conumser leisure index1, 4020
('realstonks_consumer_leis_ind2',1015), #realstonks consumer leisure index2, 4021
('realstonks_consumer_staples_ind1',1016), #realstonks consumer staples index1, 4022
('realstonks_consumer_staples_ind2',1017), #realstonks consumer staples index2, 4023
('realstonks_util_ind1',1018), #realstonks utilities index1, 4024
('realstonks_util_ind2',1019), #realstonks utilities index2, 4025
('realstonks_healthcare_ind',1020), #realstonks healthcare index, 4026
('realstonks_gold_ind',1021), #realstonks gold index, 4027
('realstonks_silver_ind',1022), #realstonks silver index, 4028
('realstonks_copper_ind',1023), #realstonks copper index, 4029
('realstonks_nat_resources_ind1',1024), #realstonks natural resources index1, 4030
('realstonks_nat_resources_ind2',1025), #realstonks natural resources index2, 4031
('realstonks_nrg_ind1',1026), #realstonks energy index1, 4032
('realstonks_nrg_ind2',1027), #realstonks energy index2, 4033
('realstonks_transport_ind1',1028), #realstonks transportation index1, 4034
('realstonks_transport_ind2',1029), #realstonks transportation index2, 4035
('realstonks_treasury_long_ind',1030), #realstonks vanguard Long Term Treasury ETF, 4036
('weather_ind',1031), #Weatherapi.com IND, 4037
('weather_hollywood',1032), #Weatherapi.com Hollywood, 4038
('weather_ny',1033), #Weatherapi.com NY, 4039
('weather_sanfran',1034), #Weatherapi.com San Fran, 4040
('weather_denver',1035), #Weatherapi.com Denver, 4041
('weather_miami',1036), #Weatherapi.com Miami, 4042
('weather_dc',1037), #Weatherapi.com DC, 4043
('weather_minneapolis',1038), #Weatherapi.com Minneapolis, 4044
('weather_houston',1039), #Weatherapi.com Houston, 4045
('weather_chi',1040) #Weatherapi.com Chicago, 4046
;

INSERT INTO cols(col, dataType, size) VALUES
#delete this when done('date_time','DATETIME', NULL), #5000 => ALL TABLES
('total','INTEGER','6'), #5000 => 4000
('price','DECIMAL','8,2'), #5001 => 4001-7
('mkt_cap','DECIMAL','18,5'), #5002 => 4001-7
('24hr_vol','DECIMAL','18,5'), #5003 => 4001-7
('mkt_cap','INTEGER','16'), #5004 => 4008
('24hr_vol','INTEGER','16'), #5005 => 4008
('btc_dom','DECIMAL','4,2'), #5006 => 4008
('crypt_num','INTEGER','5'), #5007 => 4008
('world_cnt','INTEGER','2'), #5008 => 4009
('biz_cnt','INTEGER','2'), #5009 => 4009
('climate_cnt','INTEGER','2'), #5010 => 4009
('ny_cnt','INTEGER','2'), #5011 => 4009
('brief_cnt','INTEGER','2'), #5012 => 4009
('pod_cnt','INTEGER','2'), #5013 => 4009
('oped_cnt','INTEGER','2'), #5014 => 4009
('sci_cnt','INTEGER','2'), #5015 => 4009
('arts_cnt','INTEGER','2'), #5016 => 4009
('tech_cnt','INTEGER','2'), #5017 => 4009
('finance_cnt','INTEGER','2'), #5018 => 4009
('sport_cnt','INTEGER','2'), #5019 => 4009
('us_cnt','INTEGER','2'), #5020 => 4009
('food_cnt','INTEGER','2'), #5021 => 4009
('well_cnt','INTEGER','2'), #5022 => 4009
('travel_cnt','INTEGER','2'), #5023 => 4009
('style_cnt','INTEGER','2'), #5024 => 4009
('health_cnt','INTEGER','2'), #5025 => 4009
('real_estate','INTEGER','2'), #5026 => 4009
('events','INTEGER','3'), #5027 => 4010
('avg_pop','DECIMAL','6,2'), #5028 => 4011
('adult_cnt','INTEGER','2'), #5029 => 4011
('en_cnt','INTEGER','2'), #5030 => 4011
('es_cnt','INTEGER','2'), #5031 => 4011
('ja_cnt','INTEGER','2'), #5032 => 4011,
('cnt','INTEGER','5'), #5033 => 4012
('aud','DECIMAL','3,2'), #5034 => 4013
('cad','DECIMAL','3,2'), #5035 => 4013
('cny','DECIMAL','3,2'), #5036 => 4013
('eur','DECIMAL','3,2'), #5037 => 4013
('gbp','DECIMAL','3,2'), #5038 => 4013
('hkd','DECIMAL','3,2'), #5039 => 4013
('jpy','DECIMAL','5,2'), #5040 => 4013
('is_holiday','BOOLEAN',NULL), #5041 => 4014
('score','DECIMAL','8,6'), #5042 => 4015
('ticker_all','DECIMAL','9,5'), #5043 => 4016
('ticker_aapl','DECIMAL','9,5'), #5044 => 4016
('ticker_cvx','DECIMAL','9,5'), #5045 => 4016
('ticker_dis','DECIMAL','9,5'), #5046 => 4016
('ticker_hlt','DECIMAL','9,5'), #5047 => 4016
('ticker_rblx','DECIMAL','9,5'), #5048 => 4016
('ticker_tlt','DECIMAL','9,5'), #5049 => 4016
('ticker_vti','DECIMAL','9,5'), #5050 => 4016
('price','DECIMAL','6,2'), #5051 => 4017-36
('change_pt','DECIMAL','5,2'), #5052 => 4017-36
('change_pct','DECIMAL','4,2'), #5053 => 4017-36
('volume','DECIMAL','5,2'), #5054 => 4017-36
('temp_f','DECIMAL','4,1'), #5055 => 4037-46
('cloud','INTEGER','3'), #5056 => 4037-46
('humidity','INTEGER','3') #5057 => 4037-46
;

INSERT INTO tables_cols(tablesID, colsID) VALUES
 #ADSB
(4000,5000),        #6000
#coingecko
 #ada
(4001,5001),        #6001
(4001,5002),        #6002
(4001,5003),        #6003
 #btc
(4002,5001),        #6004
(4002,5002),        #6005
(4002,5003),        #6006
 #doge
(4003,5001),        #6007
(4003,5002),        #6008
(4003,5003),        #6009
 #eth
(4004,5001),        #6010
(4004,5002),        #6011
(4004,5003),        #6012
 #ltc
(4005,5001),        #6013
(4005,5002),        #6014
(4005,5003),        #6015
 #uni
(4006,5001),        #6016
(4006,5002),        #6017
(4006,5003),        #6018
 #xmr
(4007,5001),        #6019
(4007,5002),        #6020
(4007,5003),        #6021
 #coinpaprika
(4008,5004),        #6022
(4008,5005),        #6023
(4008,5006),        #6024
(4008,5007),        #6025
 #nytimes
(4009,5008),        #6026
(4009,5009),        #6027
(4009,5010),        #6028
(4009,5011),        #6029
(4009,5012),        #6030
(4009,5013),        #6031
(4009,5014),        #6032
(4009,5015),        #6033
(4009,5016),        #6034
(4009,5017),        #6035
(4009,5018),        #6036
(4009,5019),        #6037
(4009,5020),        #6038
(4009,5021),        #6039
(4009,5022),        #6040
(4009,5023),        #6041
(4009,5024),        #6042
(4009,5025),        #6043
(4009,5026),        #6044
 #ticketmaster
(4010,5027),        #6045
 #tmdb1
(4011,5028),        #6046
(4011,5029),        #6047
(4011,5030),        #6048
(4011,5031),        #6049
(4011,5032),        #6050
 #tmdb2
(4012,5033),        #6051
 #exchange_rates
(4013,5034),        #6052
(4013,5035),        #6053
(4013,5036),        #6054
(4013,5037),        #6055
(4013,5038),        #6056
(4013,5039),        #6057
(4013,5040),        #6058
 #nager_holiday_today
(4014,5041),        #6059
 #realtor_ny_hot_score
(4015,5042),        #6060
 #twelve_data_stocks
(4016,5043),        #6061
(4016,5044),        #6062
(4016,5045),        #6063
(4016,5046),        #6064
(4016,5047),        #6065
(4016,5048),        #6066
(4016,5049),        #6067
(4016,5050),        #6068
#realstonks
 #_tech_ind
(4017,5051),        #6069
(4017,5052),        #6070
(4017,5053),        #6071
(4017,5054),        #6072
 #_financial_ind1
(4018,5051),        #6073
(4018,5052),        #6074
(4018,5053),        #6075
(4018,5054),        #6076
 #_financial_ind2
(4019,5051),        #6077
(4019,5052),        #6078
(4019,5053),        #6079
(4019,5054),        #6080
 #_consumer_leis_ind1
(4020,5051),        #6081
(4020,5052),        #6082
(4020,5053),        #6083
(4020,5054),        #6084
 #_consumser_leis_ind2
(4021,5051),        #6085
(4021,5052),        #6086
(4021,5053),        #6087
(4021,5054),        #6088
 #_consumer_staples_ind1
(4022,5051),        #6089
(4022,5052),        #6090
(4022,5053),        #6091
(4022,5054),        #6092
 #_consumer_staples_ind2
(4023,5051),        #6093
(4023,5052),        #6094
(4023,5053),        #6095
(4023,5054),        #6096
 #_util_ind1
(4024,5051),        #6097
(4024,5052),        #6098
(4024,5053),        #6099
(4024,5054),        #6100
 #_util_ind2
(4025,5051),        #6101
(4025,5052),        #6102
(4025,5053),        #6104
(4025,5054),        #6104
 #_healthcare_ind
(4026,5051),        #6105
(4026,5052),        #6106
(4026,5053),        #6107
(4026,5054),        #6108
 #_gold_ind
(4027,5051),        #6109
(4027,5052),        #6110
(4027,5053),        #6111
(4027,5054),        #6112
 #_silver_ind
(4028,5051),        #6113
(4028,5052),        #6114
(4028,5053),        #6115
(4028,5054),        #6116
 #_copper_ind
(4029,5051),        #6117
(4029,5052),        #6118
(4029,5053),        #6119
(4029,5054),        #6120
 #_nat_resources_ind1
(4030,5051),        #6121
(4030,5052),        #6122
(4030,5053),        #6123
(4030,5054),        #6124
 #_nat_resources_ind2
(4031,5051),        #6125
(4031,5052),        #6126
(4031,5053),        #6127
(4031,5054),        #6128
 #_nrg_ind1
(4032,5051),        #6129
(4032,5052),        #6130
(4032,5053),        #6131
(4032,5054),        #6132
 #_nrg_ind2
(4033,5051),        #6133
(4033,5052),        #6134
(4033,5053),        #6135
(4033,5054),        #6136
 #_transport_ind1
(4034,5051),        #6137
(4034,5052),        #6138
(4034,5053),        #6139
(4034,5054),        #6140
 #_transport_ind2
(4035,5051),        #6141
(4035,5052),        #6142
(4035,5053),        #6143
(4035,5054),        #6144
 #_treasury_long_ind
(4036,5051),        #6145
(4036,5052),        #6146
(4036,5053),        #6147
(4036,5054),        #6148
#weather_
 #ind
(4037,5055),        #6149
(4037,5056),        #6150
(4037,5057),        #6151
 #hollywood
(4038,5055),        #6152
(4038,5056),        #6153
(4038,5057),        #6154
 #ny
(4039,5055),        #6155
(4039,5056),        #6156
(4039,5057),        #6157
 #sanfran
(4040,5055),        #6158
(4040,5056),        #6159
(4040,5057),        #6160
 #denver
(4041,5055),        #6161
(4041,5056),        #6162
(4041,5057),        #6163
 #miami
(4042,5055),        #6164
(4042,5056),        #6165
(4042,5057),        #6166
 #dc
(4043,5055),        #6167
(4043,5056),        #6168
(4043,5057),        #6169
 #minneapolis
(4044,5055),        #6170
(4044,5056),        #6171
(4044,5057),        #6172
 #houston
(4045,5055),        #6173
(4045,5056),        #6174
(4045,5057),        #6175
 #chi
(4046,5055),        #6176
(4046,5056),        #6177
(4046,5057)         #6178
;

#this can really only be done after ironing out design
# -> how do we systematically push responses into columns of stonkData?

INSERT INTO header(headerCode, isTrue, tables_colsID) VALUES
 #nager_holiday_today
(204,true,6059) #8000
;

INSERT INTO vars(keyword, fn, tables_colsID) VALUES
 #adsbx
("ac","yank", 6000), #9000
#coingecko
    #ada
("cardano.usd","yank", 6001), #9001
("cardano.usd_market_cap","yank", 6002), #9002
("cardano.usd_24h_vol","yank", 6003), #9003
    #btc
("bitcoin.usd","yank", 6004), #9004
("bitcoin.usd_market_cap","yank", 6005), #9005
("bitcoin.usd_24h_vol","yank", 6006), #9006
    #doge
("dogecoin.usd","yank", 6007), #9007
("dogecoin.usd_market_cap","yank", 6008), #9008
("dogecoin.usd_24h_vol","yank", 6009), #9009
    #eth
("ethereum.usd","yank", 6010), #9010
("ethereum.usd_market_cap","yank", 6011), #9011
("ethereum.usd_24h_vol","yank", 6012), #9012
    #ltc
("litecoin.usd","yank", 6013), #9013
("litecoin.usd_market_cap","yank", 6014), #9014
("litecoin.usd_24h_vol","yank", 6015), #9015
    #uni
("uniswap.usd","yank", 6016), #9016
("uniswap.usd_market_cap","yank", 6017), #9017
("uniswap.usd_24h_vol","yank", 6018), #9018
    #xmr
("monero.usd","yank", 6019), #9019
("monero.usd_market_cap","yank", 6020), #9020
("monero.usd_24h_vol","yank", 6021), #9021
 #coinpaprika
("market_cap_usd","yank", 6022), #9022
("volume_24h_usd","yank", 6023), #9023
("bitcoin_dominance_percentage","yank", 6024), #9024
("cryptocurrencies_number","yank", 6025), #9025
 #nytimes
# this section still needs travel filled - keep pinging every day?
("results.section|world","count_var", 6026), #9026 #oh no what do i do with this? im trying pipes for split rn
("results.section|business","count_var", 6027), #9027
("results.section|climate","count_var", 6028), #9028
("results.section|nyregion","count_var", 6029), #9029
("results.section|briefing","count_var", 6030), #9030
("results.section|podcasts","count_var", 6031), #9031
("results.section|opinion","count_var", 6032), #9032
("results.section|science","count_var", 6033), #9033
("results.section|arts","count_var", 6034), #9034
("results.section|technology","count_var", 6035), #9035
("results.section|your-money","count_var", 6036), #9036
("results.section|sports","count_var", 6037), #9037
("results.section|us","count_var", 6038), #9038
("results.section|dining","count_var", 6039), #9039
("results.section|well","count_var", 6040), #9040
("results.section|travel","count_var", 6041), #9041
("results.section|style","count_var", 6042), #9042
("results.section|health","count_var", 6043), #9043
("results.section|realestate","count_var", 6044), #9044
 #ticketmaster
# this is how i might pass requirements? => may use same for nytimes?
("_embedded.events","count_tot", 6045), #9045
 #tmdb1
# this is going to need additional logic on the .split('|')
("results|popularity","avg", 6046), #9046
("results|adult","count_bool", 6047), #9047
("results|original_language|en","count_var", 6048), #9048
("results|original_language|es","count_var", 6049), #9049
("results|original_language|ja","count_var", 6050), #9050
 #tmdb2
("results","count_tot", 6051), #9051
 #exchange_rates
("rates.AUD","yank", 6052), #9052
("rates.CAD","yank", 6053), #9053
("rates.CNY","yank", 6054), #9054
("rates.EUR","yank", 6055), #9055
("rates.GBP","yank", 6056), #9056
("rates.HKD","yank", 6057), #9057
("rates.JPY","yank", 6058), #9058
 #nager_holiday_today
# skip this one for now => in headers table
 #realtor_ny_hot_score
("autocomplete._score","0th_yank", 6060), #9059
 #twelve_data_stocks
("all.price","yank", 6061), #9060
("aapl.price","yank", 6062), #9061
("cvx.price","yank", 6063), #9062
("dis.price","yank", 6064), #9063
("hlt.price","yank", 6065), #9064
("rblx.price","yank", 6066), #9065
("tlt.price","yank", 6067), #9066
("vti.price","yank", 6068), #9067
# realstonks REQUIRES python fn { data = json.loads(data) } (comes in as str)
# im currently handling transaction via pushing "json|yank" but this seems inefficient as it does not need to be done on every operation?
# this may need to be passed in somewhere with the api table or some connection table?
# => such as additional_fn or some other variable that allows weird problems like this to resolve
#
# => instead im going to do in the python if type == str { json.loads(data) }
 #realstonks
    #_tech_ind
("price","yank", 6069), #9068
("change_point","yank", 6070), #9069
("change_percentage","yank", 6071), #9070
("total_vol","yank", 6072), #9071
    #_financial_ind1
("price","yank", 6073), #9072
("change_point","yank", 6074), #9073
("change_percentage","yank", 6075), #9074
("total_vol","yank", 6076), #9075
    #_financial_ind2
("price","yank", 6077), #9076
("change_point","yank", 6078), #9077
("change_percentage","yank", 6079), #9078
("total_vol","yank", 6080), #9079
    #_consumer_leis_ind1
("price","yank", 6081), #9080
("change_point","yank", 6082), #9081
("change_percentage", "yank", 6083), #9082
("total_vol","yank", 6084), #9083
    #_consumser_leis_ind2
("price","yank", 6085), #9084
("change_point","yank", 6086), #9085
("change_percentage","yank", 6087), #9086
("total_vol","yank", 6088), #9087
    #_consumer_staples_ind1
("price","yank", 6089), #9088
("change_point","yank", 6090), #9089
("change_percentage","yank", 6091), #9090
("total_vol","yank", 6092), #9091
    #_consumer_staples_ind2
("price","yank", 6093), #9092
("change_point","yank", 6094), #9093
("change_percentage","yank", 6095), #9094
("total_vol","yank", 6096), #9095
    #_util_ind1
("price","yank", 6097), #9096
("change_point","yank", 6098), #9097
("change_percentage","yank", 6099), #9098
("total_vol","yank", 6100), #9099
    #_util_ind2
("price","yank", 6101), #9100
("change_point","yank", 6102), #9101
("change_percentage","yank", 6103), #9102
("total_vol","yank", 6104), #9103
    #_healthcare_ind
("price","yank", 6105), #9104
("change_point","yank", 6106), #9105
("change_percentage","yank", 6107), #9106
("total_vol","yank", 6108), #9107
    #_gold_ind
("price","yank", 6109), #9108
("change_point","yank", 6110), #9109
("change_percentage","yank", 6111), #9110
("total_vol","yank", 6112), #9111
    #_silver_ind
("price","yank", 6113), #9112
("change_point","yank", 6114), #9113
("change_percentage","yank", 6115), #9114
("total_vol","yank", 6116), #9115
    #_copper_ind
("price","yank", 6117), #9116
("change_point","yank", 6118), #9117
("change_percentage","yank", 6119), #9118
("total_vol","yank", 6120), #9119
    #_nat_resources_ind1
("price","yank", 6121), #9120
("change_point","yank", 6122), #9121
("change_percentage","yank", 6123), #9122
("total_vol","yank", 6124), #9123
    #_nat_resources_ind2
("price","yank", 6125), #9124
("change_point","yank", 6126), #9125
("change_percentage","yank", 6127), #9126
("total_vol","yank", 6128), #9127
    #_nrg_ind1
("price","yank", 6129), #9128
("change_point","yank", 6130), #9129
("change_percentage","yank", 6131), #9130
("total_vol","yank", 6132), #9131
    #_nrg_ind2
("price","yank", 6133), #9132
("change_point","yank", 6134), #9133
("change_percentage","yank", 6135), #9134
("total_vol","yank", 6136), #9135
    #_transport_ind1
("price","yank", 6137), #9136
("change_point","yank", 6138), #9137
("change_percentage","yank", 6139), #9138
("total_vol","yank", 6140), #9139
    #_transport_ind2
("price","yank", 6141), #9140
("change_point","yank", 6142), #9141
("change_percentage","yank", 6143), #9142
("total_vol","yank", 6144), #9143
    #_treasury_long_ind
("price","yank", 6145), #9144
("change_point","yank", 6146), #9145
("change_percentage","yank", 6147), #9146
("total_vol","yank", 6148), #9147
 #weather_
    #ind
("current.temp_f","yank", 6149), #9148
("current.cloud","yank", 6150), #9149
("current.humidity","yank", 6151), #9150
    #hollywood
("current.temp_f","yank", 6152), #9151
("current.cloud","yank", 6153), #9152
("current.humidity","yank", 6154), #9153
    #ny
("current.temp_f","yank", 6155), #9154
("current.cloud","yank", 6156), #9155
("current.humidity","yank", 6157), #9156
    #sanfran
("current.temp_f","yank", 6158), #9157
("current.cloud","yank", 6159), #9158
("current.humidity","yank", 6160), #9159
    #denver
("current.temp_f","yank", 6161), #9160
("current.cloud","yank", 6162), #9161
("current.humidity","yank", 6163), #9162
    #miami
("current.temp_f","yank", 6164), #9163
("current.cloud","yank", 6165), #9164
("current.humidity","yank", 6166), #9165
    #dc
("current.temp_f","yank", 6167), #9166
("current.cloud","yank", 6168), #9167
("current.humidity","yank", 6169), #9168
    #minneapolis
("current.temp_f","yank", 6170), #9169
("current.cloud","yank", 6171), #9170
("current.humidity","yank", 6172), #9171
    #houston
("current.temp_f","yank", 6173), #9172
("current.cloud","yank", 6174), #9173
("current.humidity","yank", 6175), #9174
    #chi
("current.temp_f","yank", 6176), #9175
("current.cloud","yank", 6177), #9176
("current.humidity","yank", 6178) #9177
;

INSERT INTO response(apiID, varsID, headerID) VALUES
 #adsbx
(1000,9000,NULL), #7000
 #coingecko
    #ada
(1001,9001,NULL), #7001
(1001,9002,NULL), #7002
(1001,9003,NULL), #7003
    #btc
(1001,9004,NULL), #7004
(1001,9005,NULL), #7005
(1001,9006,NULL), #7006
    #doge
(1001,9007,NULL), #7007
(1001,9008,NULL), #7008
(1001,9009,NULL), #7009
    #eth
(1001,9010,NULL), #7010
(1001,9011,NULL), #7011
(1001,9012,NULL), #7012
    #ltc
(1001,9013,NULL), #7013
(1001,9014,NULL), #7014
(1001,9015,NULL), #7015
    #uni
(1001,9016,NULL), #7016
(1001,9017,NULL), #7017
(1001,9018,NULL), #7018
    #xmr
(1001,9019,NULL), #7019
(1001,9020,NULL), #7020
(1001,9021,NULL), #7021
 #coinpaprika
(1002,9022,NULL), #7022
(1002,9023,NULL), #7023
(1002,9024,NULL), #7024
(1002,9025,NULL), #7025
 #nytimes
(1003,9026,NULL), #7026
(1003,9027,NULL), #7027
(1003,9028,NULL), #7028
(1003,9029,NULL), #7029
(1003,9030,NULL), #7030
(1003,9031,NULL), #7031
(1003,9032,NULL), #7032
(1003,9033,NULL), #7033
(1003,9034,NULL), #7034
(1003,9035,NULL), #7035
(1003,9036,NULL), #7036
(1003,9037,NULL), #7037
(1003,9038,NULL), #7038
(1003,9039,NULL), #7039
(1003,9040,NULL), #7040
(1003,9041,NULL), #7041
(1003,9042,NULL), #7042
(1003,9043,NULL), #7043
(1003,9044,NULL), #7044
 #ticketmaster
(1004,9045,NULL), #7045
 #tmdb1
(1005,9046,NULL), #7046
(1005,9047,NULL), #7047
(1005,9048,NULL), #7048
(1005,9049,NULL), #7049
(1005,9050,NULL), #7050
 #tmdb2
(1006,9051,NULL), #7051
 #exchange_rates
(1007,9052,NULL), #7052
(1007,9053,NULL), #7053
(1007,9054,NULL), #7054
(1007,9055,NULL), #7055
(1007,9056,NULL), #7056
(1007,9057,NULL), #7057
(1007,9058,NULL), #7058
 #nager_holiday_today
(1008,NULL,8000), #7059
 #realtor_ny_hot_score
(1009,9059,NULL), #7060
 #twelve_data_stocks
(1010,9060,NULL), #7061
(1010,9061,NULL), #7062
(1010,9062,NULL), #7063
(1010,9063,NULL), #7064
(1010,9064,NULL), #7065
(1010,9065,NULL), #7066
(1010,9066,NULL), #7067
(1010,9067,NULL), #7068
 #realstonks
    #_tech_ind
(1011,9068,NULL), #7069
(1011,9069,NULL), #7070
(1011,9070,NULL), #7071
(1011,9071,NULL), #7072
    #_financial_ind1
(1012,9072,NULL), #7073
(1012,9073,NULL), #7074
(1012,9074,NULL), #7075
(1012,9075,NULL), #7076
    #_financial_ind2
(1013,9076,NULL), #7077
(1013,9077,NULL), #7078
(1013,9078,NULL), #7079
(1013,9079,NULL), #7080
    #_consumer_leis_ind1
(1014,9080,NULL), #7081
(1014,9081,NULL), #7082
(1014,9082,NULL), #7083
(1014,9083,NULL), #7084
    #_consumser_leis_ind2
(1015,9084,NULL), #7085
(1015,9085,NULL), #7086
(1015,9086,NULL), #7087
(1015,9087,NULL), #7088
    #_consumer_staples_ind1
(1016,9088,NULL), #7089
(1016,9089,NULL), #7090
(1016,9090,NULL), #7091
(1016,9091,NULL), #7092
    #_consumer_staples_ind2
(1017,9092,NULL), #7093
(1017,9093,NULL), #7094
(1017,9094,NULL), #7095
(1017,9095,NULL), #7096
    #_util_ind1
(1018,9096,NULL), #7097
(1018,9097,NULL), #7098
(1018,9098,NULL), #7099
(1018,9099,NULL), #7100
    #_util_ind2
(1019,9100,NULL), #7101
(1019,9101,NULL), #7102
(1019,9102,NULL), #7103
(1019,9103,NULL), #7104
    #_healthcare_ind
(1020,9104,NULL), #7105
(1020,9105,NULL), #7106
(1020,9106,NULL), #7107
(1020,9107,NULL), #7108
    #_gold_ind
(1021,9108,NULL), #7109
(1021,9109,NULL), #7110
(1021,9110,NULL), #7111
(1021,9111,NULL), #7112
    #_silver_ind
(1022,9112,NULL), #7113
(1022,9113,NULL), #7114
(1022,9114,NULL), #7115
(1022,9115,NULL), #7116
    #_copper_ind
(1023,9116,NULL), #7117
(1023,9117,NULL), #7118
(1023,9118,NULL), #7119
(1023,9119,NULL), #7120
    #_nat_resources_ind1
(1024,9120,NULL), #7121
(1024,9121,NULL), #7122
(1024,9122,NULL), #7123
(1024,9123,NULL), #7124
    #_nat_resources_ind2
(1025,9124,NULL), #7125
(1025,9125,NULL), #7126
(1025,9126,NULL), #7127
(1025,9127,NULL), #7128
    #_nrg_ind1
(1026,9128,NULL), #7129
(1026,9129,NULL), #7130
(1026,9130,NULL), #7131
(1026,9131,NULL), #7132
    #_nrg_ind2
(1027,9132,NULL), #7133
(1027,9133,NULL), #7134
(1027,9134,NULL), #7135
(1027,9135,NULL), #7136
    #_transport_ind1
(1028,9136,NULL), #7137
(1028,9137,NULL), #7138
(1028,9138,NULL), #7139
(1028,9139,NULL), #7140
    #_transport_ind2
(1029,9140,NULL), #7141
(1029,9141,NULL), #7142
(1029,9142,NULL), #7143
(1029,9143,NULL), #7144
    #_treasury_long_ind
(1030,9144,NULL), #7145
(1030,9145,NULL), #7146
(1030,9146,NULL), #7147
(1030,9147,NULL), #7148
 #weather_
    #ind
(1031,9148,NULL), #7149
(1031,9149,NULL), #7150
(1031,9150,NULL), #7151
    #hollywood
(1032,9151,NULL), #7152
(1032,9152,NULL), #7153
(1032,9153,NULL), #7154
    #ny
(1033,9154,NULL), #7155
(1033,9155,NULL), #7156
(1033,9156,NULL), #7157
    #sanfran
(1034,9157,NULL), #7158
(1034,9158,NULL), #7159
(1034,9159,NULL), #7160
    #denver
(1035,9160,NULL), #7161
(1035,9161,NULL), #7162
(1035,9162,NULL), #7163
    #miami
(1036,9163,NULL), #7164
(1036,9164,NULL), #7165
(1036,9165,NULL), #7166
    #dc
(1037,9166,NULL), #7167
(1037,9167,NULL), #7168
(1037,9168,NULL), #7169
    #minneapolis
(1038,9169,NULL), #7170
(1038,9170,NULL), #7171
(1038,9171,NULL), #7172
    #houston
(1039,9172,NULL), #7173
(1039,9173,NULL), #7174
(1039,9174,NULL), #7175
    #chi
(1040,9175,NULL), #7176
(1040,9176,NULL), #7177
(1040,9177,NULL) #7178
;