#json for config and api
import json
#mysql for db connection
import mysql.connector
from mysql.connector import Error

#import config from json
try:
    with open('./flask_app/config.json') as config_file:
        config = json.load(config_file)
        db_config = config['databases']
except:
    print('config.json could not be found in application root')
#connect to the database(s)

class database:
    def __init__(self, host_name, port_num, db_name, user_name, user_pass):
        self.host = host_name
        self.port = port_num
        self.name = db_name
        self.user = user_name
        self.passwd = user_pass
        try:
            self.makeConn()
        except Error as err:
            print(f'Error: {err}')
        #self.db_conn = connection
    def makeConn(self):
        connection = mysql.connector.connect(
            host = self.host,
            port = self.port,
            database = self.name,
            user = self.user,
            password = self.passwd
        )
        self.db_conn = connection

    def fetch(self,query):
        try:
            cursor = self.db_conn.cursor()
        except:
            self.makeConn()
            cursor = self.db_conn.cursor()
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return(result)
        except Error as err:
            print(f'Error: {err}')
        
    

aggregate = database(
    host_name = db_config['aggregate']['host'],
    port_num = db_config['aggregate']['port'],
    db_name = db_config['aggregate']['name'],
    user_name = db_config['aggregate']['user'],
    user_pass = db_config['aggregate']['pass']
)