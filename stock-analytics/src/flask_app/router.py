#web app
from flask_app import app

import flask_app.config as conf

# template and serving
from flask import render_template, jsonify

#regex for filtering
import re

@ app.route('/')
def index():
    return render_template('index.html', title='Home')

@app.route('/compare')
def compare():
    return render_template('compare.html', title = 'Compare')

@app.route('/raw')
def raw():
    return render_template('raw.html', title = 'Raw Data')

@app.route('/search')
def search_results():
    return render_template('search.html', title = 'Search')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', title = '404'), 404

# api

@ app.route('/api/<hour>/<req>')
@ app.route('/api/<hour>/<req>/<negHours>')
def aggregate(hour, req, negHours = None):
    try:
        negHours = int(negHours)
    except:
        negHours = None
    if req == '*':
        headers_raw = conf.aggregate.fetch(f' select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = "aggregate{hour}" and COLUMN_NAME not like "ID"')
        header_dict = {}
        headers_str = ''
        for header in headers_raw:
            column = str(header).replace(',','').replace('(','').replace(')','').replace("'","")
            header_dict[column] = None
            headers_str+=f'{column}, '
        headers_str = headers_str[:-2]
        
        if negHours:
            sql = conf.aggregate.fetch(f'select * from aggregate{hour} WHERE date_time >= NOW() - INTERVAL {negHours} HOUR ORDER BY ID DESC')
        else:
            sql = conf.aggregate.fetch(f'select * from aggregate{hour} ORDER BY ID DESC')

        split_response = []

        for i in range(len(sql[0])):
            split_i = []
            for x in range(len(sql)):
                try:
                    split_i.append(float(sql[x][i]))
                except:
                    if re.compile('^.*-.*-.*:.*:.*$').match(str(sql[x][i])):
                        date_time_split = str(sql[x][i]).split(" ")
                        #ISO 8601 date time format
                        split_i.append(f'{date_time_split[0]}T{date_time_split[1]}.0Z')
                    else:
                        split_i.append(sql[x][i])
            split_response.append(split_i)
        iterate = 0
        for key in header_dict:
            header_dict[key] = split_response[iterate]
            iterate+=1
        to_return = jsonify(header_dict)

    elif ',' in req:
        pass
    else:
        #col_headers
        headers_raw = conf.aggregate.fetch(f'select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = "aggregate{hour}" AND COLUMN_NAME like "%{req}%"')
        header_dict = {"date_time": None}
        headers_str = ''
        for header in headers_raw:
            column = str(header).replace(',','').replace('(','').replace(')','').replace("'","")
            header_dict[column] = None
            headers_str+=f'{column}, '
        headers_str = headers_str[:-2]
        
        if negHours:
            sql = conf.aggregate.fetch(f'select date_time, {headers_str} from aggregate{hour} WHERE date_time >= NOW() - INTERVAL {negHours} HOUR ORDER BY ID DESC')
        else:
            sql = conf.aggregate.fetch(f'select date_time, {headers_str} from aggregate{hour} ORDER BY ID DESC')

        split_response = []
        
        for i in range(len(sql[0])):
            split_i = []
            for x in range(len(sql)):
                try:
                    split_i.append(float(sql[x][i]))
                except:
                    if re.compile('^.*-.*-.*:.*:.*$').match(str(sql[x][i])):
                        date_time_split = str(sql[x][i]).split(" ")
                        #ISO 8601 date time format
                        split_i.append(f'{date_time_split[0]}T{date_time_split[1]}.0Z')
                    else:
                        split_i.append(sql[x][i])
            split_response.append(split_i)
        
        iterate = 0
        for key in header_dict:
            header_dict[key] = split_response[iterate]
            iterate+=1
        to_return = jsonify(header_dict)
    return(to_return)