CELERY IMPORTS = ('tasks')
CELERY_IGNORE_RESULT = False
BROKER_HOST = '127.0.0.1'
BROKER_PORT = 56720
BROKER_URL = 'amqp://'

from datetime import timedelta

CELERYBEAT_SCHEDULE = {

}

# https://chatasweetie.com/2016/09/02/celery-distributed-task-queue/
# https://blog.miguelgrinberg.com/post/using-celery-with-flask
# https://github.com/miguelgrinberg/flask-celery-example