
import json
import requests
import mysql.connector
from mysql.connector import Error

class database:
    def __init__(self, host_name, port_num, database_name, user_name, user_password, database_connection=None):
        self.host_name = host_name
        self.port_num = port_num
        self.database_name = database_name
        self.user_name = user_name
        self.user_password = user_password
        connection = None
        try:
            self.makeConn()
        except Error as err:
            print(f"Error '{err}'")
        database_connection = connection
        self.database_connection = database_connection
    
    def makeConn(self):
        connection = mysql.connector.connect(
            host = self.host_name,
            port = self.port_num,
            database = self.database_name,
            user = self.user_name,
            password = self.user_password
        )
        self.database_connection = connection

    def fetch(self,query):
        try:
            cursor = self.database_connection.cursor()
        except:
            self.makeConn()
            cursor = self.database_connection.cursor()
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return(result)
        except Error as err:
            print(f"Error: '{err}'")

    #put fn is untested
    def put(self,query):
        try:
            cursor = self.database_connection.cursor()
        except:
            self.makeConn()
            cursor = self.database_connection.cursor()
        connection = self.database_connection
            
        try:
            cursor.execute(query)
            connection.commit()
        except Error as err:
            print(f"Error: '{err}'")
            
""" thisis old code if database.put fn has issues
def dbPut(connection,query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()
    except Error as err:
        print(f"Error: '{err}'")
"""

class apiData:
    def __init__(self, id, name, url, dayLimit, monthLimit, processing, param_vars=None,header_vars=None,table_data=None):
        self.id = id
        self.name = name
        self.url = url
        self.processing = processing
        self.param_vars = {}
        self.header_vars = {}
        self.table_data = []
        if dayLimit != None:
            #86400 secs in a day // multiply by 1.1 to add a buffer on calls to not lock out
            call_freq = 1.1 * 86400 / dayLimit
        elif monthLimit != None:
            #2419200 secs in 28 day month // multiple by 1.1 to add a buffer on calls to not lock out
            call_freq = 1.1 * 2419200 / monthLimit
        else:
            call_freq = 900
        #15 mins = 900 secs
        if call_freq < 900:
            call_freq = 900
        call_freq = round(call_freq/10)*10
        self.call_frequency = call_freq

    def push_call_vars(self, isHeader, key, value):
        #self.call_vars.append([isHeader,key,value])
        if isHeader:
            self.header_vars.update({key:value})
        else:
            self.param_vars.update({key:value})
    def push_table_data(self,table,col,keyword,func):
        self.table_data.append([table,col,keyword,func])

    def call(self):
        try:
            req = requests.get(
                self.url,
                headers = self.header_vars,
                params = self.param_vars
            )
        except:
            print("{}, {} -- requests.get error".format(self.id, self.url))
            #I would love to change this to an email function or something
            #
            # ----
            # add a return null with handling to keep the app running?
            #
            # ----
            #

        to_return = []
        if self.table_data[0][3] == True or self.table_data[0][3] == False:
            data = req.status_code
            if data == self.table_data[0][2]:
                code = True
            else:
                code = False
            to_return.append([self.table_data[0][0],self.table_data[0][1],code])
            # ^^this is used for checking for nager.date / similar
        else:
            for call_var in self.table_data:
                data = req.json()
                if isinstance(data, str):
                    data = json.loads(data)
                if call_var[3] == 'yank':
                    if isinstance(call_var[2], list):
                        for i in call_var[2]:
                            data = data[i]
                    else:
                        data = data[call_var[2]]

                elif call_var[3] == 'count_bool':
                    if isinstance(call_var[2], list):
                        pass
                    else:
                        target = call_var[2]
                        target = target.split('|')
                        data = data[target[0]]
                        tmp = 0
                        for obj in data:
                            if obj[target[1]] == True:
                                tmp += 1
                    data = tmp

                elif call_var[3] == 'count_tot':
                    if isinstance(call_var[2], list):
                        for i in call_var[2]:
                            data = data[i]
                    else:
                        data = data[call_var[2]]
                    data = len(data)

                elif call_var[3] == 'count_var':
                    if isinstance(call_var[2], list):
                        target = call_var[2].pop()
                        target = target.split('|')
                        for i in call_var[2]:
                            data = data[i]
                            tmp = 0
                            for obj in data:
                                if obj[target[0]] == target[1]:
                                    tmp += 1
                    #this likely will need modification for additional handling
                    else:
                        target = call_var[2]
                        target = target.split('|')
                        data = data[target[0]]
                        tmp = 0
                        if len(target) == 2:
                            for obj in data:
                                if obj[target[0]] == target[1]:
                                    tmp += 1
                        elif len(target) == 3:
                            for obj in data:
                                if obj[target[1]] == target[2]:
                                    tmp += 1
                        else:
                            tmp = 0
                    data = tmp

                elif call_var[3] == '0th_yank':
                    target = call_var[2]
                    data = data[target[0]]
                    data = data[0][target[1]]

                elif call_var[3] == 'avg':
                    if isinstance(call_var[2], list):
                        pass
                    else:
                        target = call_var[2]
                        target = target.split('|')
                        data = data[target[0]]
                        tmp = 0
                        for obj in data:
                            tmp += obj[target[1]]
                        tmp = tmp / len(data)
                    data = tmp

                else:
                    data = None
                #return the data with table and column information
                to_return.append([call_var[0], call_var[1], data])
        return_dict = {}
        for row_data in to_return:
            if row_data[0] not in return_dict:
                return_dict[row_data[0]] = [ '','' ]
            return_dict[row_data[0]][0] += f'{row_data[1]}, '
            return_dict[row_data[0]][1] += f'{row_data[2]}, '
            
        for key, value in return_dict.items():
            return_dict[key] = [ value[0][:-2], value[1][:-2] ]
        
        if self.processing == True:
            if self.name == 'ADSB Exchange':
                for key, val in return_dict.items():
                    return_dict[key][1] = str(val[1]).replace('None', 'NULL')
            elif 'realstonks' in self.name:
                #somewhere around here split on space and re-attach at end
                
                for key, val in return_dict.items():
                    item_list = val[1].split(' ')
                    tmp_string = ''
                    for string in item_list:
                        if 'M' in string:
                            string = string.replace('M','')
                            string = str(float(string)*1000000)
                        elif 'K' in string:
                            string = string.replace('K','')
                            string = str(float(string)*1000)
                        tmp_string+=string
                    return_dict[key][1] = tmp_string


        return(return_dict)
if __name__ == '__main__':

    stonkAPI = database('jdogg.club', 7272, 'stonkAPI', 'stonk_app_acc', '1337_5t0nk5')
    #stonkAPI.connect()
    #print(dbFetch(stonkAPI.connect(),'SELECT * FROM api LIMIT 1;'))
    selectAll = (stonkAPI.fetch('SELECT * FROM api;'))
    for i in range(len(selectAll)):
        print(i)
        print(stonkAPI.fetch('CALL getResponse({})'.format(i+1000)))