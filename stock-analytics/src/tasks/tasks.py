'''
from celery import Celery

app = Celery('tasks', broker='amp://')

@app.print_test
def test_fn():
    print('testing fn')
'''

import time
import schedule

import pkg.ingest_regest as py_db

stonkDataRegest = py_db.database('10.0.0.45', 3306, 'stonkData', 'stonk_app_acc', '1337_5t0nk5')
stonkApiIngest = py_db.database('10.0.0.45', 3306, 'stonkAPI', 'stonk_app_acc', '1337_5t0nk5')
stonkAggregateRegest = py_db.database('10.0.0.45', 3306, 'stonkShorts', 'stonk_app_acc', '1337_5t0nk5')

select_all = stonkApiIngest.fetch('SELECT * FROM api;')

# stores all objects for each api
apiDict = {}
def update_apis():
    print('updating apiDict and schedule...')
    for api in select_all:
    
        call_data = (stonkApiIngest.fetch('CALL fetchApiData({})'.format(api[0])))
        push_data = (stonkApiIngest.fetch('CALL getResponse({})'.format(api[0])))
        # api[0] = ID, api[1] = name, api[2] = url, api[4] = dayLimit, api[5] = monthLimit, api[6] = processing
        apiObject = py_db.apiData(api[0],api[1],api[2],api[4],api[5], api[6])
    
        #call_data[i][0] = pKey, call_data[i][1] = pValue, call_data[i][2] = header (boolean) 
        for i in call_data:
            apiObject.push_call_vars(i[2], i[0], i[1])
        #push_data[i][0] = table_name, push_data[i][1] = col, push_data[i][2] = keyword, push_data[i][3] = fn
        for obj in push_data:
            pointer = str(obj[2])
            if '.' in pointer:
                pointer = pointer.split('.')
            apiObject.push_table_data(obj[0], obj[1],pointer,obj[3])

        # push object into apiDict with key (api[1]) as the name for the object as the value
        apiDict.update({api[1] : apiObject})
        #apiObject.call()

        #scheduling tasks
        schedule.clear('api')
        for key, value in apiDict.items():
            schedule.every(value.call_frequency).seconds.do(call_push, api_obj_key=key, api_obj_val=value).tag('api')

#this is working code but it can NOT handle changes to the database
def aggregate(time_period):
    all_cols_types = ''
    all_cols = ''
    colDict = {}
    tables = stonkDataRegest.fetch('SHOW TABLES')
    for table in tables:
        table_data = stonkDataRegest.fetch(f'DESCRIBE {table[0]}')
        table_data.pop(0)
        for col in table_data:
            if col[1] != 'datetime':
                # appends all items to string with $ delim to create columns named col_name$row_name
                all_cols_types += (f'{table[0]}${str(col[0])} DECIMAL(20,6), ')
                all_cols += (f'{table[0]}${str(col[0])}, ')
                #gives a working dictionary of all the rows and the associated column
                if table[0] in colDict.keys():
                    colDict[table[0]] = f'{colDict[table[0]]}, {col[0]}'
                    #colDict[table[0]].append(col[0])
                else:
                    colDict[table[0]] = col[0]
                    #colDict[table[0]] = [col[0]]
    all_cols = all_cols[:-2]

    # i dont like this but lets just get it working eh?
    #stonkAggregateRegest.put(f'DROP TABLE IF EXISTS aggregate{time_period}')
    #stonkAggregateRegest.put(
    #    f'''CREATE TABLE  aggregate{time_period}(
    #        ID INTEGER(6) NOT NULL AUTO_INCREMENT,
    #        date_time DATETIME DEFAULT NOW(),
    #        {all_cols_types}
    #        PRIMARY KEY (ID)
    #    )
    #''')

    #ready for sql statements using table, row in colDict.items() => collect that last 3, 24 hours of data => average it => push it into col_name$row_name
    aggregate_push_str = ''
    for table, rows in colDict.items():
        fetch = stonkDataRegest.fetch(
        f'SELECT {rows} FROM {table} WHERE date_time >= NOW() - INTERVAL {time_period} HOUR')
        try:
            length = len(fetch[0])
        except:
            length = len(stonkDataRegest.fetch(f'DESCRIBE {table}')) - 2
        condensed_values = []
        for i in range(length):
            condensed_values.append([])
        for obj in fetch:
            for i in range(length):
                try:    
                    condensed_values[i].append(obj[i])
                except:    
                    condensed_values[i].append('NULL')
        averages = ''
        for value_set in condensed_values:
            final_value = 0
            for value in value_set:
                try:
                    final_value += value
                except:
                    pass
            try:
                final_value = final_value / len(value_set)
            except:
                final_value = 'NULL'
            averages = f'{averages}{final_value}, '
        aggregate_push_str += averages
    aggregate_push_str = aggregate_push_str[:-2]
    stonkAggregateRegest.put(f'INSERT INTO aggregate{time_period}({all_cols}) VALUES ({aggregate_push_str})')
    print(f'aggregate{time_period} updated')



def call_push(api_obj_key, api_obj_val):
    print(api_obj_key)
    try:
        pulled_data = api_obj_val.call()
        for name, values in pulled_data.items():
            stonkDataRegest.put(f'INSERT INTO {name}({values[0]}) VALUES ({values[1]})')
    except:
        print('error occurred')

if __name__ == '__main__':
    # schedule database checking every day to stay up to date and verify
    schedule.every(1).day.do(update_apis)
    # run the only schedule task (update_apis) to load all calls into objects and schedules
    schedule.run_all()

    schedule.every(3).hours.do(aggregate, time_period='3').tag('aggregate')
    schedule.every(24).hours.do(aggregate, time_period='24').tag('aggregate')

    schedule.run_all()

    while True:
        schedule.run_pending()
        time.sleep(30)
    
    '''
    print(apiObject.id)
    print(apiObject.url)
    print(apiObject.param_vars)
    print(apiObject.header_vars)
    print(apiObject.table_data)
    print(apiDict.get("Twelve Data").id)
    print("//\t//\t//\t//")
    #print(apiDict.get("CoinGecko").call())
    #apiDict.get("NYTimes").call()
    #apiDict.get("Ticketmaster").call()
    #print(apiDict.get("Nager.Date").call())
    
    pulled = apiDict.get("CoinGecko").call()
    print(pulled)
    for name, values in pulled.items():
        print(f'INSERT INTO {name}({values[0]}) VALUES ({values[1]})')
    '''
    #itm = apiDict.get('realstonks utilities index2').call()
    #for name, values in itm.items():
    #    print(f'INSERT INTO {name}({values[0]}) VALUES ({values[1]})')
    #for key, value in apiDict.items():
    #    print(value.call_frequency)
'''
    for key, value in apiDict.items():
        print(key)
        pulled_data = value.call()
        for name, values in pulled_data.items():
            stonkDataRegest.put(f'INSERT INTO {name}({values[0]}) VALUES ({values[1]})')
'''
