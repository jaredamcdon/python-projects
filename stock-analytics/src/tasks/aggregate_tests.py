import pkg.ingest_regest as py_db

stonkDataRegest = py_db.database('75.114.200.67', 7272, 'stonkData', 'stonk_app_acc', '1337_5t0nk5')
stonkApiIngest = py_db.database('75.114.200.67', 7272, 'stonkAPI', 'stonk_app_acc', '1337_5t0nk5')
stonkAggregateRegest = py_db.database('75.114.200.67', 7272, 'stonkShorts', 'stonk_app_acc', '1337_5t0nk5')

def aggregate(time_period):
    all_cols_types = ''
    all_cols = ''
    colDict = {}
    tables = stonkDataRegest.fetch('SHOW TABLES')
    for table in tables:
        table_data = stonkDataRegest.fetch(f'DESCRIBE {table[0]}')
        table_data.pop(0)
        for col in table_data:
            if col[1] != 'datetime':
                # appends all items to string with $ delim to create columns named col_name$row_name
                all_cols_types += (f'{table[0]}${str(col[0])} DECIMAL(20,6), ')
                all_cols += (f'{table[0]}${str(col[0])}, ')
                #gives a working dictionary of all the rows and the associated column
                if table[0] in colDict.keys():
                    colDict[table[0]] = f'{colDict[table[0]]}, {col[0]}'
                    #colDict[table[0]].append(col[0])
                else:
                    colDict[table[0]] = col[0]
                    #colDict[table[0]] = [col[0]]
    all_cols = all_cols[:-2]

    # i dont like this but lets just get it working eh?
    #stonkAggregateRegest.put(f'DROP TABLE IF EXISTS aggregate{time_period}')
    #stonkAggregateRegest.put(
    #    f'''CREATE TABLE  aggregate{time_period}(
    #        ID INTEGER(6) NOT NULL AUTO_INCREMENT,
    #        date_time DATETIME DEFAULT NOW(),
    #        {all_cols_types}
    #        PRIMARY KEY (ID)
    #    )
    #''')

    #ready for sql statements using table, row in colDict.items() => collect that last 3, 24 hours of data => average it => push it into col_name$row_name
    print('fetches ----')
    aggregate_push_str = ''
    for table, rows in colDict.items():
        fetch = stonkDataRegest.fetch(
        f'SELECT {rows} FROM {table} WHERE date_time >= NOW() - INTERVAL {time_period} HOUR')
        print(table + ": " + rows)
        try:
            length = len(fetch[0])
        except:
            length = len(stonkDataRegest.fetch(f'DESCRIBE {table}')) - 2
        condensed_values = []
        for i in range(length):
            condensed_values.append([])
        for obj in fetch:
            for i in range(length):
                try:    
                    condensed_values[i].append(obj[i])
                except:    
                    condensed_values[i].append('NULL')
        averages = ''
        for value_set in condensed_values:
            final_value = 0
            for value in value_set:
                try:
                    final_value += value
                except:
                    pass
            try:
                final_value = final_value / len(value_set)
            except:
                final_value = 'NULL'
            averages = f'{averages}{final_value}, '
        aggregate_push_str += averages
    aggregate_push_str = aggregate_push_str[:-2]
    stonkAggregateRegest.put(f'INSERT INTO aggregate{time_period}({all_cols}) VALUES ({aggregate_push_str})')
            
def main():
    aggregate('3')
    aggregate('24')

main()