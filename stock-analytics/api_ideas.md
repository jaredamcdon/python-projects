# Api Ideas
> Try to keep it alphabetized Jared

---

#### Air Traffic
- ADSB Exchange
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- calls per month: 360
	- Docs : https://rapidapi.com/adsbx/api/adsbexchange-com1/details 
	- Example (RapidApi Site)
```python
# This is looking at Atlanta airport long lat, busiest airport in the US

import requests

url = "https://adsbexchange-com1.p.rapidapi.com/json/lat/33.639444/lon/-84.428055/dist/250/"

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "adsbexchange-com1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)	
```

#### Company Data

- Mattermark - costs money only free trial find a replacement

### Crypto

- CoinGecko
	- No key
	- Docs: http://www.coingecko.com/api
- Coinpaprika
	- No key
	- Docs: https://api.coinpaprika.com/

#### Entertainment

- TMDB
- IMDB
- NYTimes
	- have access to books and top stories api - can get more for free
	- Key: GNLGJGSfDPTxl6ETaEkS7CbQGD6MthQu
	- example: https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=GNLGJGSfDPTxl6ETaEkS7CbQGD6MthQu
	- example: https://api.nytimes.com/svc/topstories/v2/home.json?api-key=GNLGJGSfDPTxl6ETaEkS7CbQGD6MthQu
	- docs: https://developer.nytimes.com/docs/books-product/1/overview
- Ticketmaster
	- Key: S0TTarBAAKl0L4KuNiU9XjOQoyWlCv3P
        - 5000 hits/day
        - url : https://app.ticketmaster.com/discovery/v2/
        - endpoint example: https://app.ticketmaster.com/discovery/v2/attractions/K8vZ9175BhV.json?apikey=S0TTarBAAKl0L4KuNiU9XjOQoyWlCv3P
	- Docs: http://developer.ticketmaster.com/products-and-docs/apis/getting-started/

#### Exchange Rates

- EchangeRate-Api
	- No key
	- https://api.exchangerate-api.com/v4/latest/USD

#### Financial News

- Bloomberg Market & Finance News

#### Holidays

- Nager.Date
	- No key
	- https://date.nager.at/api/v2/publicholidays/2020/US

#### Housing Data

- Realtor API
	- 500 hits/month
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- Docs: https://rapidapi.com/apidojo/api/realtor/endpoints
	- Example (Rapid API)
```python
import requests

url = "https://realtor.p.rapidapi.com/locations/auto-complete"

querystring = {"input":"new york"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "realtor.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)

```

#### News (General)

- Bing has an API look it up

#### Placebo (Junk Data)

- Super Hero Search
	- key:
	- Docs: https://superheroapi.com/
- Numbers API
- fortnite
	- https://fortnitetracker.com/site-api

#### Sports

- Api-Football
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- 100 hits/day
	- Docs: https://rapidapi.com/api-sports/api/api-football/endpoints
	- Example:
```python
import requests

url = "https://api-football-v1.p.rapidapi.com/v2/timezone"

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "api-football-v1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)
```

- Football Data
	- key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- Docs: https://rapidapi.com/BroadageSports/api/football-data1
```python
import requests

url = "https://football-data1.p.rapidapi.com/match/list/scheduled"

querystring = {"date":"07/02/2021"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "football-data1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```

- API-NBA
	- 100 hits/day
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- Docs: https://rapidapi.com/api-sports/api/api-nba
	- Example:
```python
import requests

url = "https://api-nba-v1.p.rapidapi.com/games/date/2021-03-27"

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "api-nba-v1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)
```
- __compare to predictions?__

### Stocks

- Twelve Data
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- 800 hits/day
	- docs: https://rapidapi.com/twelvedata/api/twelve-data1
	- example (rapid api):
```python
import requests

url = "https://twelve-data1.p.rapidapi.com/stocks"

querystring = {"exchange":"NASDAQ","format":"json"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "twelve-data1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```

- yahoo finance
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- 500 hits/month
	- Docs: https://rapidapi.com/apidojo/api/yahoo-finance1
	- Example (Rapid Api):
```python
import requests

url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/auto-complete"

querystring = {"q":"tesla","region":"US"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```
- Alpha Vantage
	- 500 hits/day
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- Docs: https://www.alphavantage.co/
	- Example (Rapid API):
```python
import requests

url = "https://alpha-vantage.p.rapidapi.com/query"

querystring = {"function":"GLOBAL_QUOTE","symbol":"TSLA"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "alpha-vantage.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```
- IEX Cloud
	- Key: 
	- Docs: https://iexcloud.io/docs/api/
- marketstack
	- 1000 hits/month
	- key: 
	- https://marketstack.com/


#### Weather

- Open weather map
	- 500 hits/month
	- key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- docs: https://rapidapi.com/community/api/open-weather-map/endpoints
	- Example (Rapid Api):
```python
import requests

url = "https://community-open-weather-map.p.rapidapi.com/climate/month"

querystring = {"q":"San Francisco"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```
- Weatherbit
	- Key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- 125 hits/day
	- Docs: https://rapidapi.com/weatherbit/api/weather/endpoints
	- Example (Rapid Api):
```python
import requests

url = "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/minutely"

querystring = {"lat":"35.5","lon":"-78.5"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "weatherbit-v1-mashape.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```

- WeatherAPI.com
	- 1,000,000 hits/mo
	- key: e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d
	- Docs: https://rapidapi.com/weatherapi/api/weatherapi-com
	- example (rapidapi):
```python
import requests

url = "https://weatherapi-com.p.rapidapi.com/search.json"

querystring = {"q":"<REQUIRED>"}

headers = {
    'x-rapidapi-key': "e43d58c8ffmsh1d3365a6dbea4fdp15fb03jsnefed60e3831d",
    'x-rapidapi-host': "weatherapi-com.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)
```

---

##
