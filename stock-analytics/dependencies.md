# Dependencies
> currently using a pipenv for the backend, db is just mysql/maria, no frontend yet

## Pip
 - pipenv
    - used for controlling packages
 - celery
    - for making async api calls, looking for other options as well
 - flask
    - for hosting backend
 - requests
    - for api calls
