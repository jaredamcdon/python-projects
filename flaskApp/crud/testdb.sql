CREATE DATABASE	crudDB;

CREATE TABLE crudTable(
id DECIMAL(4,0) PRIMARY KEY,
firstName VARCHAR(20),
lastName VARCHAR(20)
);

INSERT INTO crudTable VALUES
(1, 'Jared', 'McDonald'),
(2, 'Travis', 'Scott')
;