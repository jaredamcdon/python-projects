from app import app
from flask import MySQL

mysql = MySQL()

#mysql configuration
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD']= ''
app.config['MYSQL_DATABASE_DB'] = 'crudDB'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)
