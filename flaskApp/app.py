from flask import Flask, render_template

app = Flask(__name__)

members = ['Jared', 'Jardok', 'Diggory']



@app.route('/')
def index():
    return render_template('/index.html')
@app.route('/members')
def members():
    return render_template('member.html')

if __name__ == '__main__':
    app.run(debug=True, host = '0.0.0.0')
