import sqlite3
import argparse
import csv

parser = argparse.ArgumentParser()

parser.add_argument("-d", "--inputDB")
parser.add_argument("-o", "--outputCSV")

args = parser.parse_args()

database = args.inputDB

CSV = args.outputCSV

print(args.inputDB)

conn = sqlite3.connect(database)

#conn.execute('.tables')

def getTables(conn):
    cursor = conn.execute("SELECT name FROM sqlite_master WHERE type='table' AND name NOT LIKE '%spellfix%';")
    tables = [
        v[0] for v in cursor.fetchall()
        if v[0] != "sqlite_sequence"
    ]
    cursor.close()
    return tables

def getHeaders(tables):
    headerObj = []
    for table in tables:
        cursor = conn.execute("PRAGMA table_info({})".format(table))
        headers = cursor.fetchall()
        headerObj.append(headers)
    newHeaderObj = []
    headerListObj  = list()
    for obj in headerObj:
        for indvObj in obj:
            headerListObj.append([indvObj[1]])
        headerTupleObj = tuple(headerListObj)
    newHeaderObj.append(headerTupleObj)
    print(newHeaderObj)
    return(newHeaderObj)

def selectFromAll(tables, conn):
    dataObj = []
    for table in tables:
        sql = "SELECT * FROM {}".format(table)
        cursor = conn.execute(sql)
        #cursor = conn.execute("SELECT * FROM directories;")
        dataObj.append(cursor.fetchall())
    return dataObj

def writeToCSV(tables, selectStatements, columnNames, CSV):
    csvFile = open(CSV, 'w')
    with csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(columnNames)
        for item in selectStatements:
            writer.writerows(item)

def manualCommand(conn):
    sqlStatement = input("sql> ")
    cursor = conn.execute(sqlStatement)
    output = cursor.fetchall()
    cursor.close()
    return output

tables = getTables(conn)

print(tables)

columnNames = getHeaders(tables)

selectStatements = selectFromAll(tables, conn)

writeToCSV(tables, selectStatements, columnNames, CSV)

enterStatements = input("run additional commands? [Y/n] ")

def askContinue():
    if enterStatements == "n" or enterStatements == "N":
        keepGoing = False
    else:
        keepGoing = True
    return keepGoing

keepGoing = askContinue()

while keepGoing:
    output = manualCommand(conn)
    print(output)
    keepGoing = askContinue()
