# Enter your code here. The csv input has been populated for you into a string, csv_input. Print output to STDOUT




def createNested():
    #break string into individual lines
    csvLines = str.splitlines(csv_input)

    #remove header
    csvLines.pop(0)

    splitLines = []
    for i in range(len(csvLines)):
        #break csvLines into individual columns
        splitLines.append(csvLines[i].split(','))
    return(splitLines)

def scrubCols(splitLines):
    for i in range(len(splitLines)):
        #remove excess from process => ticker
        splitLines[i][1] = str(splitLines[i][1].split('_')[0])
        #remove excess (host,log)
        splitLines[i] = [splitLines[i][0],splitLines[i][1],splitLines[i][4]]
    return(splitLines)
        
def findDuplicates(splitLines):
    for i in range(len(splitLines)):
        testList = splitLines[:i] + ['pass'] + splitLines[i+1:]
    
        for j in range(len(testList)):
            if splitLines[i][1] == testList[j][1] and splitLines[i][0] == testList[j][0]:
                out = int(splitLines[j][2]) + int(splitLines[i][2])
                splitLines[j][2] = str(out)
                splitLines[i][2] = '0'
    return(splitLines)

def clearNulls(splitLines):
    toReturn=[]
    for i in range(len(splitLines)):
        if splitLines[i][2] == '0':
            pass
        else:
            toReturn.append(splitLines[i])
        #return(toReturn)
    return(toReturn)
    
def printOut(finalOutput):
    print('date,exchange,total_bytes')
    for col in finalOutput:
        print('{},{},{}'.format(col[0],col[1],col[2]))
        
if __name__ == "__main__":
    splitLines = createNested()
    splitLines = scrubCols(splitLines)
    splitLines = findDuplicates(splitLines)
    splitLines = clearNulls(splitLines)
    printOut(splitLines)
